package com.seemovies.sdk.mvp.base;

/**
 * Created by Jarchie on 2017\11\7.
 * 描述：所有View的基类
 */

public interface BaseView {
    void showLoading();

    void dismissLoading();
}
