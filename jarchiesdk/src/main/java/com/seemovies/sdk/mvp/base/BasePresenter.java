package com.seemovies.sdk.mvp.base;

import io.reactivex.disposables.Disposable;

/**
 * Created by Jarchie on 2017\11\7 0007.
 * 描述：Presenter的基类
 */

public interface BasePresenter {
    //默认初始化
    void start();

    //Activity关闭时把View对象置为空
    void detach();

    //将网络请求的每一个disposable添加进CompositeDisposable，在退出时一并注销
    void addDisposable(Disposable subscription);

    //注销所有请求
    void unDisposable();
}
