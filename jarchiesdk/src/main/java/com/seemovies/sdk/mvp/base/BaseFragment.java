package com.seemovies.sdk.mvp.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.seemovies.sdk.utils.BackHandlerHelper;
import com.seemovies.sdk.utils.FragmentBackHandler;

/**
 * Created by Jarchie on 2017\11\7.
 * 描述：所有Fragment的基类
 */

@SuppressWarnings("unused")
public abstract class BaseFragment<P extends BasePresenter> extends Fragment implements BaseView, FragmentBackHandler {
    protected View mRootView;
    protected P presenter;
    private boolean isViewCreate = false; //view是否创建
    private boolean isViewVisiable = false; //view是否可见
    public Context mContext;
    private boolean isFirst = true; //是否是第一次加载

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = initPresenter();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        isViewCreate = true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getLayoutId() != 0) {
            mRootView = inflater.inflate(getLayoutId(), container, false);
        }
        initView(mRootView);
        setListener();
        initData();
        return mRootView;
    }

    //此方法描述的是： 获取布局
    protected abstract int getLayoutId();

    //此方法描述的是： 初始化界面
    protected abstract void initView(View rootView);

    //此方法描述的是： 初始化监听事件
    protected abstract void setListener();

    //此方法描述的是： 初始化数据
    protected abstract void initData();

    @SuppressWarnings("unchecked")
    public <T extends View> T obtainView(int resId) {
        return (T) mRootView.findViewById(resId);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isViewVisiable = isVisibleToUser;
        if (isVisibleToUser && isViewCreate) {
            visiableToUser();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isViewVisiable) {
            visiableToUser();
        }
    }

    //懒加载，让用户可见第一次加载
    protected void firstLoad() {
    }

    //懒加载，让用户可见
    protected void visiableToUser() {
        if (isFirst) {
            firstLoad();
            isFirst = false;
        }
    }

    @Override
    public void onDestroyView() {
        if (presenter != null) {
            presenter.detach();
        }
        isViewCreate = false;
        super.onDestroyView();
    }

    /**
     * 在子类中初始化对应的presenter
     *
     * @return 相应的presenter
     */
    public abstract P initPresenter();

    @Override
    public void showLoading() {}

    @Override
    public void dismissLoading() {}

    //不需要处理Fragment返回键的不用重写该方法
    @Override
    public boolean onBackPressed() {
        return BackHandlerHelper.handleBackPress(this);
    }

}
