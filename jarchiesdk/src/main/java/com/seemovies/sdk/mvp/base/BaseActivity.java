package com.seemovies.sdk.mvp.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.seemovies.sdk.utils.ActivityManager;

/**
 * Created by Jarchie on 2017\11\7.
 * 描述：所有Activity的基类
 */

public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity implements BaseView {
    protected P presenter;
    public Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        ImmersionBar.with(this).init(); //初始化，默认透明状态栏和黑色导航栏
        mContext = this;
        if (getLayoutId() != 0) {
            setContentView(getLayoutId());
        }
        //将当前Activity添加到管理栈中
        ActivityManager.getInstance().addActivity(this);
        presenter = initPresenter();
        initView();
        setListener();
        initData();
    }

    /**
     * 返回当前界面布局文件
     *
     * @return
     * @Title:getLayoutId
     * @Description:返回布局
     * @return:int
     */
    protected abstract int getLayoutId();

    //此方法描述的是： 初始化所有view
    protected abstract void initView();

    //此方法描述的是： 设置所有事件监听
    protected abstract void setListener();

    //此方法描述的是： 初始化数据
    protected abstract void initData();

    /**
     * 采用泛型实现的绑定控件的方法,无需再findViewById
     * 直接传入控件的id即可
     */
    @SuppressWarnings("unchecked")
    public <T extends View> T obtainView(int resId) {
        return (T) findViewById(resId);
    }

    @Override
    protected void onDestroy() {
        //不调用该方法，如果界面bar发生改变，在不关闭app的情况下，退出此界面再进入将记忆最后一次bar改变的状态
//        ImmersionBar.with(this).destroy();
        //将当前Activity移除出管理栈
        ActivityManager.getInstance().removeActivity(this);
        if (presenter != null) {
            presenter.detach(); //在presenter中解绑释放view
            presenter = null;
        }
        super.onDestroy();
    }

    /**
     * 在子类中初始化对应的presenter
     *
     * @return 相应的presenter
     */
    public abstract P initPresenter();

    @Override
    public void dismissLoading() {}

    @Override
    public void showLoading() {}

}
