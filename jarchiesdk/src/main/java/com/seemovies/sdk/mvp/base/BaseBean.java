package com.seemovies.sdk.mvp.base;

/**
 * Created by Jarchie on 2017\11\7.
 * 描述：请求结果基础Bean类，仅用于判断操作是否成功
 */

@SuppressWarnings("unused")
public class BaseBean<T> {
    private String code;
    private String message;
    private T data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
