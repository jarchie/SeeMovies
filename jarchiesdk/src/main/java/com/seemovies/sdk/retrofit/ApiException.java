package com.seemovies.sdk.retrofit;

/**
 * Created by Jarchie on 2017\11\7.
 * 描述：自定义异常
 */

@SuppressWarnings({"unused", "FieldCanBeLocal", "WeakerAccess"})
public class ApiException extends RuntimeException {
    private int code;

    public ApiException(String message) {
        super(new Throwable(message));
    }

    public ApiException(Throwable throwable, int code) {
        super(throwable);
        this.code = code;
    }

}
