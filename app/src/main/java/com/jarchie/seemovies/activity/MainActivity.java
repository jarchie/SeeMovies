package com.jarchie.seemovies.activity;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.mvp.contact.MainContact;
import com.jarchie.seemovies.mvp.presenter.MainPresenter;
import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationView;
import com.luseen.luseenbottomnavigation.BottomNavigation.OnBottomNavigationItemClickListener;
import com.seemovies.sdk.mvp.base.BaseActivity;

/**
 * Created by Jarchie on 2017\11\7.
 * 描述：项目的主活动页面
 */
public class MainActivity extends BaseActivity<MainContact.presenter> implements OnBottomNavigationItemClickListener {
    private BottomNavigationView mBottomNavigationView;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        mBottomNavigationView = obtainView(R.id.navigation_view);
    }

    @Override
    protected void setListener() {
        mBottomNavigationView.setOnBottomNavigationItemClickListener(this);
    }

    @Override
    protected void initData() {
        presenter.initBottomNavigationView(this,mBottomNavigationView);
        presenter.setSelect(this,0);
    }

    @Override
    public MainContact.presenter initPresenter() {
        return new MainPresenter(this);
    }

    @Override
    public void onNavigationItemClick(int index) {
        switch (index) {
            case 0:
                presenter.setSelect(this,0);
                break;
            case 1:
                presenter.setSelect(this,1);
                break;
            case 2:
                presenter.setSelect(this,2);
                break;
            case 3:
                presenter.setSelect(this,3);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        presenter.exitApp(this);
    }

}
