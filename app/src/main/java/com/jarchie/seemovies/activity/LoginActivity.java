package com.jarchie.seemovies.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.bean.RegisterEvent;
import com.jarchie.seemovies.mvp.contact.LoginContact;
import com.jarchie.seemovies.mvp.presenter.LoginPresenter;
import com.seemovies.sdk.mvp.base.BaseActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Jarchie on 2017\11\15.
 * 描述：系统登录
 */

@SuppressWarnings("unused")
public class LoginActivity extends BaseActivity<LoginContact.presenter> implements View.OnClickListener {
    private Toolbar mToolBar;
    private TextInputLayout userNameLayout, passWordLayout;
    private TextView btnGoRegister;
    private Button btnLogin;
    private EditText editUserName, editPwd;
    private CheckBox mCheckBox;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this); //注册事件
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {
        mToolBar = obtainView(R.id.toolbar);
        userNameLayout = obtainView(R.id.layout_username);
        passWordLayout = obtainView(R.id.layout_password);
        btnGoRegister = obtainView(R.id.tv_go_register);
        btnLogin = obtainView(R.id.btn_login);
        editUserName = obtainView(R.id.edit_username);
        editPwd = obtainView(R.id.edit_password);
        mCheckBox = obtainView(R.id.save_pwd);
        presenter.initSaveState(this, mCheckBox, editUserName, editPwd); //初始化CheckBox的选中状态
    }

    @Override
    protected void setListener() {
        mToolBar.setOnClickListener(this);
        btnGoRegister.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    @Override
    protected void initData() {
        presenter.initHint(this, userNameLayout, passWordLayout);
    }

    @Override
    public LoginContact.presenter initPresenter() {
        return new LoginPresenter(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar:
                finish();
                break;
            case R.id.btn_login:
                presenter.login(this, mCheckBox, editUserName, editPwd);
                break;
            case R.id.tv_go_register:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRegisterEvent(RegisterEvent event) {
        LoginActivity.this.finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this); //取消事件
    }

}
