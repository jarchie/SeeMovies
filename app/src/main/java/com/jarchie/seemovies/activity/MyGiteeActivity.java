package com.jarchie.seemovies.activity;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.helper.LoadingH5Page;
import com.jarchie.seemovies.utils.Constant;
import com.seemovies.sdk.mvp.base.BaseActivity;
import com.seemovies.sdk.mvp.base.BasePresenter;

/**
 * Created by Jarchie on 2017\11\17.
 * 描述：我的码云页面
 */

public class MyGiteeActivity extends BaseActivity implements View.OnClickListener {
    private Toolbar mToolBar;
    private ProgressBar mProgressbar;
    private WebView mWebview;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_gitee;
    }

    @Override
    protected void initView() {
        mToolBar = (Toolbar) obtainView(R.id.toolbar);
        mProgressbar = (ProgressBar) obtainView(R.id.mProgressBar);
        mWebview = (WebView) obtainView(R.id.mWebView);
    }

    @Override
    protected void setListener() {
        mToolBar.setOnClickListener(this);
    }

    @Override
    protected void initData() {
        LoadingH5Page.handleWebView(mWebview, mProgressbar, Constant.MYGITEE_URL);
    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar:
                finish();
                break;
        }
    }

}
