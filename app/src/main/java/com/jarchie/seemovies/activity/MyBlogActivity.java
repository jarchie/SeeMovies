package com.jarchie.seemovies.activity;

import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.helper.LoadingH5Page;
import com.jarchie.seemovies.utils.CommonUtils;
import com.jarchie.seemovies.utils.Constant;
import com.seemovies.sdk.mvp.base.BaseActivity;
import com.seemovies.sdk.mvp.base.BasePresenter;

/**
 * Created by Jarchie on 2017\11\17.
 * 描述：我的博客页面
 */

public class MyBlogActivity extends BaseActivity implements View.OnClickListener {
    private Toolbar mToolBar;
    private ProgressBar mProgressbar;
    private WebView mWebview;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_blog;
    }

    @Override
    protected void initView() {
        mToolBar = (Toolbar) obtainView(R.id.toolbar);
        mProgressbar = (ProgressBar) obtainView(R.id.mProgressBar);
        mWebview = (WebView) obtainView(R.id.mWebView);
    }

    @Override
    protected void setListener() {
        mToolBar.setOnClickListener(this);
    }

    @Override
    protected void initData() {
        LoadingH5Page.handleWebView(mWebview, mProgressbar, Constant.MYBLOG_URL);
    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar: //自定义的返回按钮
                CommonUtils.backPreviousPage(MyBlogActivity.this,Constant.BACK_PREVIOUS_ACTIVITY, KeyEvent.KEYCODE_BACK, mWebview);
                break;
        }
    }

    //处理系统返回键
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mWebview.canGoBack()) {
                mWebview.goBack();//返回上一页面
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

}
