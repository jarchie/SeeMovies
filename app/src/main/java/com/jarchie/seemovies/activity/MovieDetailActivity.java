package com.jarchie.seemovies.activity;

import android.app.ProgressDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.adapter.IndexCommentAdapter;
import com.jarchie.seemovies.mvp.contact.MovieDetailContact;
import com.jarchie.seemovies.mvp.model.MovieDetailBean;
import com.jarchie.seemovies.mvp.presenter.MovieDetailPresenter;
import com.jarchie.seemovies.views.ExpandableTextView;
import com.seemovies.sdk.glide.GlideImageView;
import com.seemovies.sdk.mvp.base.BaseActivity;
import com.xw.repo.BubbleSeekBar;

/**
 * Created by Jarchie on 2017\11\9.
 * 描述：电影模块的详情页
 */

@SuppressWarnings("deprecation")
public class MovieDetailActivity extends BaseActivity<MovieDetailContact.presenter> implements MovieDetailContact.view, View.OnClickListener {
    private Toolbar mToolBar;
    private TextView name, type, country, length, date, score, sum;
    private BubbleSeekBar mSeekBar;
    private ExpandableTextView mExpandTextView;
    private GlideImageView mImageView;
    private RecyclerView mRecyclerView;
    private ProgressDialog dialog;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_movie_detail;
    }

    @Override
    protected void initView() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("加载中。。。");
        mToolBar = obtainView(R.id.toolbar);
        name = obtainView(R.id.detail_nm);
        type = obtainView(R.id.detail_cat);
        country = obtainView(R.id.detail_src);
        length = obtainView(R.id.detail_dur);
        date = obtainView(R.id.detail_rt);
        score = obtainView(R.id.detail_sc);
        sum = obtainView(R.id.detail_snum);
        mSeekBar = obtainView(R.id.detail_seekbar);
        mExpandTextView = obtainView(R.id.detail_dra);
        mImageView = obtainView(R.id.detail_img);
        mRecyclerView = obtainView(R.id.detail_recycler);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mRecyclerView.setHasFixedSize(true);
    }

    @Override
    protected void setListener() {
        mToolBar.setOnClickListener(this);
    }

    @Override
    protected void initData() {
        int id = getIntent().getIntExtra("id", -1);
        presenter.getData(id);
    }

    @Override
    public MovieDetailContact.presenter initPresenter() {
        return new MovieDetailPresenter(this);
    }

    @Override
    public void setData(MovieDetailBean.DataBean dataBean) {
        name.setText(dataBean.getMovieDetailModel().getNm()); //名称
        type.setText(dataBean.getMovieDetailModel().getCat()); //类型
        country.setText(dataBean.getMovieDetailModel().getSrc()); //国家
        length.setText(" / " + dataBean.getMovieDetailModel().getDur() + "分钟"); //长度
        date.setText(dataBean.getMovieDetailModel().getRt()); //日期
        score.setText(String.valueOf(dataBean.getMovieDetailModel().getSc())); //分数
        sum.setText("观影：" + dataBean.getMovieDetailModel().getSnum() + "人"); //人数
        mImageView.loadImage(dataBean.getMovieDetailModel().getImg(), R.color.placeholder_color); //加载图片
        mExpandTextView.setText(dataBean.getMovieDetailModel().getDra().trim()); //电影描述
        mSeekBar.setProgress(dataBean.getMovieDetailModel().getSc()); //设置进度条
        IndexCommentAdapter mAdapter = new IndexCommentAdapter(this, dataBean.getCommentResponseModel().getHcmts());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar: //关闭页面
                finish();
                break;
        }
    }

    @Override
    public void showLoading() {
        super.showLoading();
        dialog.show();
    }

    @Override
    public void dismissLoading() {
        super.dismissLoading();
        dialog.dismiss();
    }

}
