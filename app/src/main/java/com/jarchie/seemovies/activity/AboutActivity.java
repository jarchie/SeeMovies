package com.jarchie.seemovies.activity;

import android.support.v7.widget.Toolbar;
import android.view.View;

import com.jarchie.seemovies.R;
import com.seemovies.sdk.mvp.base.BaseActivity;
import com.seemovies.sdk.mvp.base.BasePresenter;

/**
 * Created by Jarchie on 2017\11\17.
 * 描述：关于页面
 */

public class AboutActivity extends BaseActivity implements View.OnClickListener {
    private Toolbar mToolBar;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_about;
    }

    @Override
    protected void initView() {
        mToolBar = (Toolbar) obtainView(R.id.toolbar);
    }

    @Override
    protected void setListener() {
        mToolBar.setOnClickListener(this);
    }

    @Override
    protected void initData() {

    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar:
                finish();
                break;
        }
    }

}
