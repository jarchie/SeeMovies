package com.jarchie.seemovies.activity;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.mvp.contact.RegisterContact;
import com.jarchie.seemovies.mvp.presenter.RegisterPresenter;
import com.seemovies.sdk.mvp.base.BaseActivity;

/**
 * Created by Jarchie on 2017\11\15.
 * 描述：注册页面
 */

public class RegisterActivity extends BaseActivity<RegisterContact.presenter> implements View.OnClickListener {
    private Toolbar mToolBar;
    private TextInputLayout userNameLayout, passWordLayout;
    private TextView btnGoLogin;
    private Button btnRegister;
    private EditText editUserName, editPwd;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_register;
    }

    @Override
    protected void initView() {
        mToolBar = obtainView(R.id.toolbar);
        userNameLayout = obtainView(R.id.layout_username);
        passWordLayout = obtainView(R.id.layout_password);
        btnGoLogin = obtainView(R.id.tv_go_login);
        btnRegister = obtainView(R.id.btn_register);
        editUserName = obtainView(R.id.edit_username);
        editPwd = obtainView(R.id.edit_password);
    }

    @Override
    protected void setListener() {
        btnGoLogin.setOnClickListener(this);
        mToolBar.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
    }

    @Override
    protected void initData() {
        presenter.setHint(this, userNameLayout, passWordLayout);
    }

    @Override
    public RegisterContact.presenter initPresenter() {
        return new RegisterPresenter(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar: //关闭页面
                finish();
                break;
            case R.id.tv_go_login: //返回登录页
                finish();
                break;
            case R.id.btn_register: //注册
                presenter.register(this, editUserName, editPwd);
                break;
        }
    }

}
