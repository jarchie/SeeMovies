package com.jarchie.seemovies.activity;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.mvp.contact.SplashContact;
import com.jarchie.seemovies.mvp.presenter.SplashPresenter;
import com.jarchie.seemovies.utils.CommonUtils;
import com.seemovies.sdk.mvp.base.BaseActivity;

/**
 * Created by Jarchie on 2017\11\21.
 * 描述：项目欢迎页面
 */

public class SplashActivity extends BaseActivity<SplashContact.presenter> implements View.OnClickListener {
    private LinearLayout skipLayout;
    private TextView skipTextView;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initView() {
        skipLayout = obtainView(R.id.layout_skip);
        skipTextView = obtainView(R.id.tv_skip);
        TextView welcomeTextView = obtainView(R.id.tv_welcome);
        CommonUtils.setFont(this, welcomeTextView);
    }

    @Override
    protected void setListener() {
        skipLayout.setOnClickListener(this);
    }

    @Override
    protected void initData() {
        presenter.skipMainPage(this, skipTextView);
    }

    @Override
    public SplashContact.presenter initPresenter() {
        return new SplashPresenter(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_skip:
                presenter.clickSkipMainPage(this, skipTextView);
                break;
        }
    }

}
