package com.jarchie.seemovies.mvp.presenter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;

import com.jarchie.seemovies.BaseApplication;
import com.jarchie.seemovies.R;
import com.jarchie.seemovies.activity.MainActivity;
import com.jarchie.seemovies.fragment.CenimaFragment;
import com.jarchie.seemovies.fragment.MineFragment;
import com.jarchie.seemovies.fragment.MovieFragment;
import com.jarchie.seemovies.fragment.WanAndroidFragment;
import com.jarchie.seemovies.mvp.contact.MainContact;
import com.jarchie.seemovies.utils.Constant;
import com.jarchie.seemovies.utils.TipUtils;
import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationItem;
import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationView;
import com.seemovies.sdk.mvp.base.BaseView;
import com.seemovies.sdk.mvp.impl.BasePresenterImpl;
import com.seemovies.sdk.utils.BackHandlerHelper;

/**
 * Created by Jarchie on 2017\11\24.
 * 描述：主页业务逻辑的处理类
 */

@SuppressWarnings({"unchecked", "ConstantConditions"})
public class MainPresenter extends BasePresenterImpl implements MainContact.presenter {
    private Fragment movieFgt, cenimaFgt, tuijianFgt, mineFgt; //Fragment
    private long exitTime = 0; //退出APP的时间

    public MainPresenter(BaseView view) {
        super(view);
    }

    @Override
    public void initBottomNavigationView(Context context, BottomNavigationView mBottomNavigationView) {
        if (mBottomNavigationView != null) {
            //设置汉字是否一直显示
            mBottomNavigationView.isWithText(true);
            //整体背景色，false是icon和文字显示颜色能用,此时是默认的颜色
            mBottomNavigationView.isColoredBackground(false);
            //设置选中时的颜色
            mBottomNavigationView.setItemActiveColorWithoutColoredBackground(ContextCompat.getColor(context, R.color.colorPrimary));
        }
        BottomNavigationItem movieItem = new BottomNavigationItem(context.getString(R.string.movie), ContextCompat.getColor(context, R.color.colorPrimary), R.drawable.movie_unpress);
        BottomNavigationItem cenimaItem = new BottomNavigationItem(context.getString(R.string.cinema), ContextCompat.getColor(context, R.color.colorPrimary), R.drawable.yy_unpress);
        BottomNavigationItem tuijianItem = new BottomNavigationItem(context.getString(R.string.tuijian), ContextCompat.getColor(context, R.color.colorPrimary), R.drawable.android_unpress);
        BottomNavigationItem mineItem = new BottomNavigationItem(context.getString(R.string.mine), ContextCompat.getColor(context, R.color.colorPrimary), R.drawable.mine_unpress);
        mBottomNavigationView.addTab(movieItem);
        mBottomNavigationView.addTab(cenimaItem);
        mBottomNavigationView.addTab(tuijianItem);
        mBottomNavigationView.addTab(mineItem);
    }

    @Override
    public void setSelect(MainActivity context, int pos) {
        FragmentTransaction transaction = context.getSupportFragmentManager().beginTransaction();
        if (movieFgt != null) {
            transaction.hide(movieFgt);
        }
        if (cenimaFgt != null) {
            transaction.hide(cenimaFgt);
        }
        if (tuijianFgt != null) {
            transaction.hide(tuijianFgt);
        }
        if (mineFgt != null) {
            transaction.hide(mineFgt);
        }
        switch (pos) {
            case 0:
                if (movieFgt == null) {
                    movieFgt = new MovieFragment();
                    transaction.add(R.id.id_content, movieFgt);
                } else {
                    transaction.show(movieFgt);
                }
                break;
            case 1:
                if (cenimaFgt == null) {
                    cenimaFgt = new CenimaFragment();
                    transaction.add(R.id.id_content, cenimaFgt);
                } else {
                    transaction.show(cenimaFgt);
                }
                break;
            case 2:
                if (tuijianFgt == null) {
                    tuijianFgt = new WanAndroidFragment();
                    transaction.add(R.id.id_content, tuijianFgt);
                } else {
                    transaction.show(tuijianFgt);
                }
                break;
            case 3:
                if (mineFgt == null) {
                    mineFgt = new MineFragment();
                    transaction.add(R.id.id_content, mineFgt);
                } else {
                    transaction.show(mineFgt);
                }
                break;
        }
        transaction.commit();
    }

    @Override
    public void exitApp(MainActivity context) {
        if (!BackHandlerHelper.handleBackPress(context)) {
            if ((System.currentTimeMillis() - exitTime) > Constant.EXIT_TIME) {
                TipUtils.showToast(BaseApplication.getAppContext(), "再按一次退出程序");
                exitTime = System.currentTimeMillis();
            } else {
                context.finish();
                //结束进程
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
        }
    }

}
