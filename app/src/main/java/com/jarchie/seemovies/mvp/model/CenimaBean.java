package com.jarchie.seemovies.mvp.model;

import java.util.List;

/**
 * Created by Jarchie on 2017\11\17.
 * 描述：影院列表的实体
 */

@SuppressWarnings("ALL")
public class CenimaBean {

    private ControlBean control;
    private int status;
    private DataBean data;

    public ControlBean getControl() {
        return control;
    }

    public void setControl(ControlBean control) {
        this.control = control;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ControlBean {
        /**
         * expires : 43200
         */

        private int expires;

        public int getExpires() {
            return expires;
        }

        public void setExpires(int expires) {
            this.expires = expires;
        }
    }

    @SuppressWarnings("unused")
    public static class DataBean {
        private List<江干区Bean> 江干区;
        private List<余杭区Bean> 余杭区;
        private List<滨江区Bean> 滨江区;
        private List<萧山区Bean> 萧山区;
        private List<下城区Bean> 下城区;
        private List<西湖区Bean> 西湖区;
        private List<拱墅区Bean> 拱墅区;
        private List<富阳区Bean> 富阳区;
        private List<上城区Bean> 上城区;
        private List<桐庐县Bean> 桐庐县;
        private List<临安市Bean> 临安市;
        private List<建德市Bean> 建德市;
        private List<淳安县Bean> 淳安县;

        public List<江干区Bean> get江干区() {
            return 江干区;
        }

        public void set江干区(List<江干区Bean> 江干区) {
            this.江干区 = 江干区;
        }

        public List<余杭区Bean> get余杭区() {
            return 余杭区;
        }

        public void set余杭区(List<余杭区Bean> 余杭区) {
            this.余杭区 = 余杭区;
        }

        public List<滨江区Bean> get滨江区() {
            return 滨江区;
        }

        public void set滨江区(List<滨江区Bean> 滨江区) {
            this.滨江区 = 滨江区;
        }

        public List<萧山区Bean> get萧山区() {
            return 萧山区;
        }

        public void set萧山区(List<萧山区Bean> 萧山区) {
            this.萧山区 = 萧山区;
        }

        public List<下城区Bean> get下城区() {
            return 下城区;
        }

        public void set下城区(List<下城区Bean> 下城区) {
            this.下城区 = 下城区;
        }

        public List<西湖区Bean> get西湖区() {
            return 西湖区;
        }

        public void set西湖区(List<西湖区Bean> 西湖区) {
            this.西湖区 = 西湖区;
        }

        public List<拱墅区Bean> get拱墅区() {
            return 拱墅区;
        }

        public void set拱墅区(List<拱墅区Bean> 拱墅区) {
            this.拱墅区 = 拱墅区;
        }

        public List<富阳区Bean> get富阳区() {
            return 富阳区;
        }

        public void set富阳区(List<富阳区Bean> 富阳区) {
            this.富阳区 = 富阳区;
        }

        public List<上城区Bean> get上城区() {
            return 上城区;
        }

        public void set上城区(List<上城区Bean> 上城区) {
            this.上城区 = 上城区;
        }

        public List<桐庐县Bean> get桐庐县() {
            return 桐庐县;
        }

        public void set桐庐县(List<桐庐县Bean> 桐庐县) {
            this.桐庐县 = 桐庐县;
        }

        public List<临安市Bean> get临安市() {
            return 临安市;
        }

        public void set临安市(List<临安市Bean> 临安市) {
            this.临安市 = 临安市;
        }

        public List<建德市Bean> get建德市() {
            return 建德市;
        }

        public void set建德市(List<建德市Bean> 建德市) {
            this.建德市 = 建德市;
        }

        public List<淳安县Bean> get淳安县() {
            return 淳安县;
        }

        public void set淳安县(List<淳安县Bean> 淳安县) {
            this.淳安县 = 淳安县;
        }

        public static class 江干区Bean {
            /**
             * sellPrice : 39.0
             * area : 江干区
             * imax : 1
             * lat : 30.309969
             * lng : 120.32624
             * addr : 江干区金沙大道560号金沙天街商业中心5楼
             * brd : SFC上影影城
             * dis :
             * deal : 0
             * distance : 0
             * follow : 0
             * nm : SFC上影影城(龙湖IMAX店)
             * ct :
             * poiId : 67948019
             * preferential : 0
             * sellmin : 0
             * sell : true
             * id : 13669
             * brdId : 2880988
             * dealPrice : 0.0
             * referencePrice : 0.0
             * showCount : 0
             */

            private double sellPrice;
            private String area;
            private int imax;
            private double lat;
            private double lng;
            private String addr;
            private String brd;
            private String dis;
            private int deal;
            private int distance;
            private int follow;
            private String nm;
            private String ct;
            private int poiId;
            private int preferential;
            private int sellmin;
            private boolean sell;
            private int id;
            private int brdId;
            private double dealPrice;
            private double referencePrice;
            private int showCount;

            public double getSellPrice() {
                return sellPrice;
            }

            public void setSellPrice(double sellPrice) {
                this.sellPrice = sellPrice;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public int getImax() {
                return imax;
            }

            public void setImax(int imax) {
                this.imax = imax;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public String getAddr() {
                return addr;
            }

            public void setAddr(String addr) {
                this.addr = addr;
            }

            public String getBrd() {
                return brd;
            }

            public void setBrd(String brd) {
                this.brd = brd;
            }

            public String getDis() {
                return dis;
            }

            public void setDis(String dis) {
                this.dis = dis;
            }

            public int getDeal() {
                return deal;
            }

            public void setDeal(int deal) {
                this.deal = deal;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public int getFollow() {
                return follow;
            }

            public void setFollow(int follow) {
                this.follow = follow;
            }

            public String getNm() {
                return nm;
            }

            public void setNm(String nm) {
                this.nm = nm;
            }

            public String getCt() {
                return ct;
            }

            public void setCt(String ct) {
                this.ct = ct;
            }

            public int getPoiId() {
                return poiId;
            }

            public void setPoiId(int poiId) {
                this.poiId = poiId;
            }

            public int getPreferential() {
                return preferential;
            }

            public void setPreferential(int preferential) {
                this.preferential = preferential;
            }

            public int getSellmin() {
                return sellmin;
            }

            public void setSellmin(int sellmin) {
                this.sellmin = sellmin;
            }

            public boolean isSell() {
                return sell;
            }

            public void setSell(boolean sell) {
                this.sell = sell;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getBrdId() {
                return brdId;
            }

            public void setBrdId(int brdId) {
                this.brdId = brdId;
            }

            public double getDealPrice() {
                return dealPrice;
            }

            public void setDealPrice(double dealPrice) {
                this.dealPrice = dealPrice;
            }

            public double getReferencePrice() {
                return referencePrice;
            }

            public void setReferencePrice(double referencePrice) {
                this.referencePrice = referencePrice;
            }

            public int getShowCount() {
                return showCount;
            }

            public void setShowCount(int showCount) {
                this.showCount = showCount;
            }
        }

        public static class 余杭区Bean {
            /**
             * sellPrice : 24.0
             * area : 余杭区
             * imax : 0
             * lat : 30.415155
             * lng : 120.29865
             * addr : 余杭区临平南苑街道藕花洲大街303号沃尔玛4楼
             * brd : SFC上影影城
             * dis : 余杭
             * deal : 0
             * distance : 0
             * follow : 0
             * nm : SFC上影影城(临平店)
             * ct :
             * poiId : 90293
             * preferential : 0
             * sellmin : 0
             * sell : true
             * id : 1356
             * brdId : 2880988
             * dealPrice : 0.0
             * referencePrice : 60.0
             * showCount : 0
             */

            private double sellPrice;
            private String area;
            private int imax;
            private double lat;
            private double lng;
            private String addr;
            private String brd;
            private String dis;
            private int deal;
            private int distance;
            private int follow;
            private String nm;
            private String ct;
            private int poiId;
            private int preferential;
            private int sellmin;
            private boolean sell;
            private int id;
            private int brdId;
            private double dealPrice;
            private double referencePrice;
            private int showCount;

            public double getSellPrice() {
                return sellPrice;
            }

            public void setSellPrice(double sellPrice) {
                this.sellPrice = sellPrice;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public int getImax() {
                return imax;
            }

            public void setImax(int imax) {
                this.imax = imax;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public String getAddr() {
                return addr;
            }

            public void setAddr(String addr) {
                this.addr = addr;
            }

            public String getBrd() {
                return brd;
            }

            public void setBrd(String brd) {
                this.brd = brd;
            }

            public String getDis() {
                return dis;
            }

            public void setDis(String dis) {
                this.dis = dis;
            }

            public int getDeal() {
                return deal;
            }

            public void setDeal(int deal) {
                this.deal = deal;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public int getFollow() {
                return follow;
            }

            public void setFollow(int follow) {
                this.follow = follow;
            }

            public String getNm() {
                return nm;
            }

            public void setNm(String nm) {
                this.nm = nm;
            }

            public String getCt() {
                return ct;
            }

            public void setCt(String ct) {
                this.ct = ct;
            }

            public int getPoiId() {
                return poiId;
            }

            public void setPoiId(int poiId) {
                this.poiId = poiId;
            }

            public int getPreferential() {
                return preferential;
            }

            public void setPreferential(int preferential) {
                this.preferential = preferential;
            }

            public int getSellmin() {
                return sellmin;
            }

            public void setSellmin(int sellmin) {
                this.sellmin = sellmin;
            }

            public boolean isSell() {
                return sell;
            }

            public void setSell(boolean sell) {
                this.sell = sell;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getBrdId() {
                return brdId;
            }

            public void setBrdId(int brdId) {
                this.brdId = brdId;
            }

            public double getDealPrice() {
                return dealPrice;
            }

            public void setDealPrice(double dealPrice) {
                this.dealPrice = dealPrice;
            }

            public double getReferencePrice() {
                return referencePrice;
            }

            public void setReferencePrice(double referencePrice) {
                this.referencePrice = referencePrice;
            }

            public int getShowCount() {
                return showCount;
            }

            public void setShowCount(int showCount) {
                this.showCount = showCount;
            }
        }

        public static class 滨江区Bean {
            /**
             * sellPrice : 34.0
             * area : 滨江区
             * imax : 1
             * lat : 30.202332
             * lng : 120.20426
             * addr : 滨江区长河街道江汉路1515号龙湖滨江天街商业中心5F-Z01，4F-Z02
             * brd : CGV星星国际影城
             * dis :
             * deal : 0
             * distance : 0
             * follow : 0
             * nm : CGV影城(滨江店)
             * ct :
             * poiId : 162782339
             * preferential : 0
             * sellmin : 0
             * sell : true
             * id : 24082
             * brdId : 23941
             * dealPrice : 0.0
             * referencePrice : 0.0
             * showCount : 0
             */

            private double sellPrice;
            private String area;
            private int imax;
            private double lat;
            private double lng;
            private String addr;
            private String brd;
            private String dis;
            private int deal;
            private int distance;
            private int follow;
            private String nm;
            private String ct;
            private int poiId;
            private int preferential;
            private int sellmin;
            private boolean sell;
            private int id;
            private int brdId;
            private double dealPrice;
            private double referencePrice;
            private int showCount;

            public double getSellPrice() {
                return sellPrice;
            }

            public void setSellPrice(double sellPrice) {
                this.sellPrice = sellPrice;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public int getImax() {
                return imax;
            }

            public void setImax(int imax) {
                this.imax = imax;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public String getAddr() {
                return addr;
            }

            public void setAddr(String addr) {
                this.addr = addr;
            }

            public String getBrd() {
                return brd;
            }

            public void setBrd(String brd) {
                this.brd = brd;
            }

            public String getDis() {
                return dis;
            }

            public void setDis(String dis) {
                this.dis = dis;
            }

            public int getDeal() {
                return deal;
            }

            public void setDeal(int deal) {
                this.deal = deal;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public int getFollow() {
                return follow;
            }

            public void setFollow(int follow) {
                this.follow = follow;
            }

            public String getNm() {
                return nm;
            }

            public void setNm(String nm) {
                this.nm = nm;
            }

            public String getCt() {
                return ct;
            }

            public void setCt(String ct) {
                this.ct = ct;
            }

            public int getPoiId() {
                return poiId;
            }

            public void setPoiId(int poiId) {
                this.poiId = poiId;
            }

            public int getPreferential() {
                return preferential;
            }

            public void setPreferential(int preferential) {
                this.preferential = preferential;
            }

            public int getSellmin() {
                return sellmin;
            }

            public void setSellmin(int sellmin) {
                this.sellmin = sellmin;
            }

            public boolean isSell() {
                return sell;
            }

            public void setSell(boolean sell) {
                this.sell = sell;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getBrdId() {
                return brdId;
            }

            public void setBrdId(int brdId) {
                this.brdId = brdId;
            }

            public double getDealPrice() {
                return dealPrice;
            }

            public void setDealPrice(double dealPrice) {
                this.dealPrice = dealPrice;
            }

            public double getReferencePrice() {
                return referencePrice;
            }

            public void setReferencePrice(double referencePrice) {
                this.referencePrice = referencePrice;
            }

            public int getShowCount() {
                return showCount;
            }

            public void setShowCount(int showCount) {
                this.showCount = showCount;
            }
        }

        public static class 萧山区Bean {
            /**
             * sellPrice : 34.0
             * area : 萧山区
             * imax : 0
             * lat : 30.194666
             * lng : 120.253525
             * addr : 萧山区金鸡路与建设一路交口宝龙广场4楼
             * brd : 大地影院
             * dis :
             * deal : 0
             * distance : 0
             * follow : 0
             * nm : 大地影院(宝龙广场店)
             * ct :
             * poiId : 91523698
             * preferential : 0
             * sellmin : 0
             * sell : true
             * id : 14989
             * brdId : 384262
             * dealPrice : 0.0
             * referencePrice : 0.0
             * showCount : 0
             */

            private double sellPrice;
            private String area;
            private int imax;
            private double lat;
            private double lng;
            private String addr;
            private String brd;
            private String dis;
            private int deal;
            private int distance;
            private int follow;
            private String nm;
            private String ct;
            private int poiId;
            private int preferential;
            private int sellmin;
            private boolean sell;
            private int id;
            private int brdId;
            private double dealPrice;
            private double referencePrice;
            private int showCount;

            public double getSellPrice() {
                return sellPrice;
            }

            public void setSellPrice(double sellPrice) {
                this.sellPrice = sellPrice;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public int getImax() {
                return imax;
            }

            public void setImax(int imax) {
                this.imax = imax;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public String getAddr() {
                return addr;
            }

            public void setAddr(String addr) {
                this.addr = addr;
            }

            public String getBrd() {
                return brd;
            }

            public void setBrd(String brd) {
                this.brd = brd;
            }

            public String getDis() {
                return dis;
            }

            public void setDis(String dis) {
                this.dis = dis;
            }

            public int getDeal() {
                return deal;
            }

            public void setDeal(int deal) {
                this.deal = deal;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public int getFollow() {
                return follow;
            }

            public void setFollow(int follow) {
                this.follow = follow;
            }

            public String getNm() {
                return nm;
            }

            public void setNm(String nm) {
                this.nm = nm;
            }

            public String getCt() {
                return ct;
            }

            public void setCt(String ct) {
                this.ct = ct;
            }

            public int getPoiId() {
                return poiId;
            }

            public void setPoiId(int poiId) {
                this.poiId = poiId;
            }

            public int getPreferential() {
                return preferential;
            }

            public void setPreferential(int preferential) {
                this.preferential = preferential;
            }

            public int getSellmin() {
                return sellmin;
            }

            public void setSellmin(int sellmin) {
                this.sellmin = sellmin;
            }

            public boolean isSell() {
                return sell;
            }

            public void setSell(boolean sell) {
                this.sell = sell;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getBrdId() {
                return brdId;
            }

            public void setBrdId(int brdId) {
                this.brdId = brdId;
            }

            public double getDealPrice() {
                return dealPrice;
            }

            public void setDealPrice(double dealPrice) {
                this.dealPrice = dealPrice;
            }

            public double getReferencePrice() {
                return referencePrice;
            }

            public void setReferencePrice(double referencePrice) {
                this.referencePrice = referencePrice;
            }

            public int getShowCount() {
                return showCount;
            }

            public void setShowCount(int showCount) {
                this.showCount = showCount;
            }
        }

        public static class 下城区Bean {
            /**
             * sellPrice : 34.0
             * area : 下城区
             * imax : 0
             * lat : 30.26198
             * lng : 120.16064
             * addr : 下城区龙游路38号
             * brd : 其它
             * dis : 武林商圈
             * deal : 0
             * distance : 0
             * follow : 0
             * nm : 奥斯卡电影大世界
             * ct :
             * poiId : 689906
             * preferential : 0
             * sellmin : 0
             * sell : true
             * id : 1367
             * brdId : 0
             * dealPrice : 0.0
             * referencePrice : 50.0
             * showCount : 0
             */

            private double sellPrice;
            private String area;
            private int imax;
            private double lat;
            private double lng;
            private String addr;
            private String brd;
            private String dis;
            private int deal;
            private int distance;
            private int follow;
            private String nm;
            private String ct;
            private int poiId;
            private int preferential;
            private int sellmin;
            private boolean sell;
            private int id;
            private int brdId;
            private double dealPrice;
            private double referencePrice;
            private int showCount;

            public double getSellPrice() {
                return sellPrice;
            }

            public void setSellPrice(double sellPrice) {
                this.sellPrice = sellPrice;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public int getImax() {
                return imax;
            }

            public void setImax(int imax) {
                this.imax = imax;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public String getAddr() {
                return addr;
            }

            public void setAddr(String addr) {
                this.addr = addr;
            }

            public String getBrd() {
                return brd;
            }

            public void setBrd(String brd) {
                this.brd = brd;
            }

            public String getDis() {
                return dis;
            }

            public void setDis(String dis) {
                this.dis = dis;
            }

            public int getDeal() {
                return deal;
            }

            public void setDeal(int deal) {
                this.deal = deal;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public int getFollow() {
                return follow;
            }

            public void setFollow(int follow) {
                this.follow = follow;
            }

            public String getNm() {
                return nm;
            }

            public void setNm(String nm) {
                this.nm = nm;
            }

            public String getCt() {
                return ct;
            }

            public void setCt(String ct) {
                this.ct = ct;
            }

            public int getPoiId() {
                return poiId;
            }

            public void setPoiId(int poiId) {
                this.poiId = poiId;
            }

            public int getPreferential() {
                return preferential;
            }

            public void setPreferential(int preferential) {
                this.preferential = preferential;
            }

            public int getSellmin() {
                return sellmin;
            }

            public void setSellmin(int sellmin) {
                this.sellmin = sellmin;
            }

            public boolean isSell() {
                return sell;
            }

            public void setSell(boolean sell) {
                this.sell = sell;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getBrdId() {
                return brdId;
            }

            public void setBrdId(int brdId) {
                this.brdId = brdId;
            }

            public double getDealPrice() {
                return dealPrice;
            }

            public void setDealPrice(double dealPrice) {
                this.dealPrice = dealPrice;
            }

            public double getReferencePrice() {
                return referencePrice;
            }

            public void setReferencePrice(double referencePrice) {
                this.referencePrice = referencePrice;
            }

            public int getShowCount() {
                return showCount;
            }

            public void setShowCount(int showCount) {
                this.showCount = showCount;
            }
        }

        public static class 西湖区Bean {
            /**
             * sellPrice : 32.0
             * area : 西湖区
             * imax : 0
             * lat : 30.162329
             * lng : 120.0703
             * addr : 西湖区转塘街道横桥路98号金都艺墅商业综合楼三楼304~307、312~329室
             * brd : 其它
             * dis :
             * deal : 0
             * distance : 0
             * follow : 0
             * nm : DFM国际影城(绿城之江一号店)
             * ct :
             * poiId : 157344478
             * preferential : 0
             * sellmin : 0
             * sell : true
             * id : 17373
             * brdId : 0
             * dealPrice : 0.0
             * referencePrice : 0.0
             * showCount : 0
             */

            private double sellPrice;
            private String area;
            private int imax;
            private double lat;
            private double lng;
            private String addr;
            private String brd;
            private String dis;
            private int deal;
            private int distance;
            private int follow;
            private String nm;
            private String ct;
            private int poiId;
            private int preferential;
            private int sellmin;
            private boolean sell;
            private int id;
            private int brdId;
            private double dealPrice;
            private double referencePrice;
            private int showCount;

            public double getSellPrice() {
                return sellPrice;
            }

            public void setSellPrice(double sellPrice) {
                this.sellPrice = sellPrice;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public int getImax() {
                return imax;
            }

            public void setImax(int imax) {
                this.imax = imax;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public String getAddr() {
                return addr;
            }

            public void setAddr(String addr) {
                this.addr = addr;
            }

            public String getBrd() {
                return brd;
            }

            public void setBrd(String brd) {
                this.brd = brd;
            }

            public String getDis() {
                return dis;
            }

            public void setDis(String dis) {
                this.dis = dis;
            }

            public int getDeal() {
                return deal;
            }

            public void setDeal(int deal) {
                this.deal = deal;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public int getFollow() {
                return follow;
            }

            public void setFollow(int follow) {
                this.follow = follow;
            }

            public String getNm() {
                return nm;
            }

            public void setNm(String nm) {
                this.nm = nm;
            }

            public String getCt() {
                return ct;
            }

            public void setCt(String ct) {
                this.ct = ct;
            }

            public int getPoiId() {
                return poiId;
            }

            public void setPoiId(int poiId) {
                this.poiId = poiId;
            }

            public int getPreferential() {
                return preferential;
            }

            public void setPreferential(int preferential) {
                this.preferential = preferential;
            }

            public int getSellmin() {
                return sellmin;
            }

            public void setSellmin(int sellmin) {
                this.sellmin = sellmin;
            }

            public boolean isSell() {
                return sell;
            }

            public void setSell(boolean sell) {
                this.sell = sell;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getBrdId() {
                return brdId;
            }

            public void setBrdId(int brdId) {
                this.brdId = brdId;
            }

            public double getDealPrice() {
                return dealPrice;
            }

            public void setDealPrice(double dealPrice) {
                this.dealPrice = dealPrice;
            }

            public double getReferencePrice() {
                return referencePrice;
            }

            public void setReferencePrice(double referencePrice) {
                this.referencePrice = referencePrice;
            }

            public int getShowCount() {
                return showCount;
            }

            public void setShowCount(int showCount) {
                this.showCount = showCount;
            }
        }

        public static class 拱墅区Bean {
            /**
             * sellPrice : 34.0
             * area : 拱墅区
             * imax : 0
             * lat : 30.308603
             * lng : 120.139694
             * addr : 拱墅区丽水路58号乐堤港5楼
             * brd : CGV星星国际影城
             * dis :
             * deal : 0
             * distance : 0
             * follow : 0
             * nm : CGV影城(运河店)
             * ct :
             * poiId : 159539917
             * preferential : 0
             * sellmin : 0
             * sell : true
             * id : 23789
             * brdId : 23941
             * dealPrice : 0.0
             * referencePrice : 0.0
             * showCount : 0
             */

            private double sellPrice;
            private String area;
            private int imax;
            private double lat;
            private double lng;
            private String addr;
            private String brd;
            private String dis;
            private int deal;
            private int distance;
            private int follow;
            private String nm;
            private String ct;
            private int poiId;
            private int preferential;
            private int sellmin;
            private boolean sell;
            private int id;
            private int brdId;
            private double dealPrice;
            private double referencePrice;
            private int showCount;

            public double getSellPrice() {
                return sellPrice;
            }

            public void setSellPrice(double sellPrice) {
                this.sellPrice = sellPrice;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public int getImax() {
                return imax;
            }

            public void setImax(int imax) {
                this.imax = imax;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public String getAddr() {
                return addr;
            }

            public void setAddr(String addr) {
                this.addr = addr;
            }

            public String getBrd() {
                return brd;
            }

            public void setBrd(String brd) {
                this.brd = brd;
            }

            public String getDis() {
                return dis;
            }

            public void setDis(String dis) {
                this.dis = dis;
            }

            public int getDeal() {
                return deal;
            }

            public void setDeal(int deal) {
                this.deal = deal;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public int getFollow() {
                return follow;
            }

            public void setFollow(int follow) {
                this.follow = follow;
            }

            public String getNm() {
                return nm;
            }

            public void setNm(String nm) {
                this.nm = nm;
            }

            public String getCt() {
                return ct;
            }

            public void setCt(String ct) {
                this.ct = ct;
            }

            public int getPoiId() {
                return poiId;
            }

            public void setPoiId(int poiId) {
                this.poiId = poiId;
            }

            public int getPreferential() {
                return preferential;
            }

            public void setPreferential(int preferential) {
                this.preferential = preferential;
            }

            public int getSellmin() {
                return sellmin;
            }

            public void setSellmin(int sellmin) {
                this.sellmin = sellmin;
            }

            public boolean isSell() {
                return sell;
            }

            public void setSell(boolean sell) {
                this.sell = sell;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getBrdId() {
                return brdId;
            }

            public void setBrdId(int brdId) {
                this.brdId = brdId;
            }

            public double getDealPrice() {
                return dealPrice;
            }

            public void setDealPrice(double dealPrice) {
                this.dealPrice = dealPrice;
            }

            public double getReferencePrice() {
                return referencePrice;
            }

            public void setReferencePrice(double referencePrice) {
                this.referencePrice = referencePrice;
            }

            public int getShowCount() {
                return showCount;
            }

            public void setShowCount(int showCount) {
                this.showCount = showCount;
            }
        }

        public static class 富阳区Bean {
            /**
             * sellPrice : 28.0
             * area : 富阳区
             * imax : 0
             * lat : 30.04846
             * lng : 119.94791
             * addr : 富阳区富春街道桂花西路82号
             * brd : 其它
             * dis :
             * deal : 0
             * distance : 0
             * follow : 0
             * nm : OAC影城
             * ct :
             * poiId : 115508290
             * preferential : 0
             * sellmin : 0
             * sell : true
             * id : 16424
             * brdId : 0
             * dealPrice : 0.0
             * referencePrice : 0.0
             * showCount : 0
             */

            private double sellPrice;
            private String area;
            private int imax;
            private double lat;
            private double lng;
            private String addr;
            private String brd;
            private String dis;
            private int deal;
            private int distance;
            private int follow;
            private String nm;
            private String ct;
            private int poiId;
            private int preferential;
            private int sellmin;
            private boolean sell;
            private int id;
            private int brdId;
            private double dealPrice;
            private double referencePrice;
            private int showCount;

            public double getSellPrice() {
                return sellPrice;
            }

            public void setSellPrice(double sellPrice) {
                this.sellPrice = sellPrice;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public int getImax() {
                return imax;
            }

            public void setImax(int imax) {
                this.imax = imax;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public String getAddr() {
                return addr;
            }

            public void setAddr(String addr) {
                this.addr = addr;
            }

            public String getBrd() {
                return brd;
            }

            public void setBrd(String brd) {
                this.brd = brd;
            }

            public String getDis() {
                return dis;
            }

            public void setDis(String dis) {
                this.dis = dis;
            }

            public int getDeal() {
                return deal;
            }

            public void setDeal(int deal) {
                this.deal = deal;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public int getFollow() {
                return follow;
            }

            public void setFollow(int follow) {
                this.follow = follow;
            }

            public String getNm() {
                return nm;
            }

            public void setNm(String nm) {
                this.nm = nm;
            }

            public String getCt() {
                return ct;
            }

            public void setCt(String ct) {
                this.ct = ct;
            }

            public int getPoiId() {
                return poiId;
            }

            public void setPoiId(int poiId) {
                this.poiId = poiId;
            }

            public int getPreferential() {
                return preferential;
            }

            public void setPreferential(int preferential) {
                this.preferential = preferential;
            }

            public int getSellmin() {
                return sellmin;
            }

            public void setSellmin(int sellmin) {
                this.sellmin = sellmin;
            }

            public boolean isSell() {
                return sell;
            }

            public void setSell(boolean sell) {
                this.sell = sell;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getBrdId() {
                return brdId;
            }

            public void setBrdId(int brdId) {
                this.brdId = brdId;
            }

            public double getDealPrice() {
                return dealPrice;
            }

            public void setDealPrice(double dealPrice) {
                this.dealPrice = dealPrice;
            }

            public double getReferencePrice() {
                return referencePrice;
            }

            public void setReferencePrice(double referencePrice) {
                this.referencePrice = referencePrice;
            }

            public int getShowCount() {
                return showCount;
            }

            public void setShowCount(int showCount) {
                this.showCount = showCount;
            }
        }

        public static class 上城区Bean {
            /**
             * sellPrice : 24.0
             * area : 上城区
             * imax : 0
             * lat : 30.228107
             * lng : 120.169395
             * addr : 上城区中山南路77号尚城1157.利星名品广场4楼
             * brd : 其它
             * dis :
             * deal : 0
             * distance : 0
             * follow : 0
             * nm : 保利院线尚橙电影工场
             * ct :
             * poiId : 100850352
             * preferential : 0
             * sellmin : 0
             * sell : true
             * id : 16154
             * brdId : 0
             * dealPrice : 0.0
             * referencePrice : 0.0
             * showCount : 0
             */

            private double sellPrice;
            private String area;
            private int imax;
            private double lat;
            private double lng;
            private String addr;
            private String brd;
            private String dis;
            private int deal;
            private int distance;
            private int follow;
            private String nm;
            private String ct;
            private int poiId;
            private int preferential;
            private int sellmin;
            private boolean sell;
            private int id;
            private int brdId;
            private double dealPrice;
            private double referencePrice;
            private int showCount;

            public double getSellPrice() {
                return sellPrice;
            }

            public void setSellPrice(double sellPrice) {
                this.sellPrice = sellPrice;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public int getImax() {
                return imax;
            }

            public void setImax(int imax) {
                this.imax = imax;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public String getAddr() {
                return addr;
            }

            public void setAddr(String addr) {
                this.addr = addr;
            }

            public String getBrd() {
                return brd;
            }

            public void setBrd(String brd) {
                this.brd = brd;
            }

            public String getDis() {
                return dis;
            }

            public void setDis(String dis) {
                this.dis = dis;
            }

            public int getDeal() {
                return deal;
            }

            public void setDeal(int deal) {
                this.deal = deal;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public int getFollow() {
                return follow;
            }

            public void setFollow(int follow) {
                this.follow = follow;
            }

            public String getNm() {
                return nm;
            }

            public void setNm(String nm) {
                this.nm = nm;
            }

            public String getCt() {
                return ct;
            }

            public void setCt(String ct) {
                this.ct = ct;
            }

            public int getPoiId() {
                return poiId;
            }

            public void setPoiId(int poiId) {
                this.poiId = poiId;
            }

            public int getPreferential() {
                return preferential;
            }

            public void setPreferential(int preferential) {
                this.preferential = preferential;
            }

            public int getSellmin() {
                return sellmin;
            }

            public void setSellmin(int sellmin) {
                this.sellmin = sellmin;
            }

            public boolean isSell() {
                return sell;
            }

            public void setSell(boolean sell) {
                this.sell = sell;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getBrdId() {
                return brdId;
            }

            public void setBrdId(int brdId) {
                this.brdId = brdId;
            }

            public double getDealPrice() {
                return dealPrice;
            }

            public void setDealPrice(double dealPrice) {
                this.dealPrice = dealPrice;
            }

            public double getReferencePrice() {
                return referencePrice;
            }

            public void setReferencePrice(double referencePrice) {
                this.referencePrice = referencePrice;
            }

            public int getShowCount() {
                return showCount;
            }

            public void setShowCount(int showCount) {
                this.showCount = showCount;
            }
        }

        public static class 桐庐县Bean {
            /**
             * sellPrice : 38.0
             * area : 桐庐县
             * imax : 0
             * lat : 29.78799
             * lng : 119.697105
             * addr : 桐庐县迎春南路69号
             * brd : 其它
             * dis : 桐庐
             * deal : 0
             * distance : 0
             * follow : 0
             * nm : 晨光国际影城
             * ct :
             * poiId : 1500386
             * preferential : 0
             * sellmin : 0
             * sell : true
             * id : 2322
             * brdId : 0
             * dealPrice : 0.0
             * referencePrice : 50.0
             * showCount : 0
             */

            private double sellPrice;
            private String area;
            private int imax;
            private double lat;
            private double lng;
            private String addr;
            private String brd;
            private String dis;
            private int deal;
            private int distance;
            private int follow;
            private String nm;
            private String ct;
            private int poiId;
            private int preferential;
            private int sellmin;
            private boolean sell;
            private int id;
            private int brdId;
            private double dealPrice;
            private double referencePrice;
            private int showCount;

            public double getSellPrice() {
                return sellPrice;
            }

            public void setSellPrice(double sellPrice) {
                this.sellPrice = sellPrice;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public int getImax() {
                return imax;
            }

            public void setImax(int imax) {
                this.imax = imax;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public String getAddr() {
                return addr;
            }

            public void setAddr(String addr) {
                this.addr = addr;
            }

            public String getBrd() {
                return brd;
            }

            public void setBrd(String brd) {
                this.brd = brd;
            }

            public String getDis() {
                return dis;
            }

            public void setDis(String dis) {
                this.dis = dis;
            }

            public int getDeal() {
                return deal;
            }

            public void setDeal(int deal) {
                this.deal = deal;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public int getFollow() {
                return follow;
            }

            public void setFollow(int follow) {
                this.follow = follow;
            }

            public String getNm() {
                return nm;
            }

            public void setNm(String nm) {
                this.nm = nm;
            }

            public String getCt() {
                return ct;
            }

            public void setCt(String ct) {
                this.ct = ct;
            }

            public int getPoiId() {
                return poiId;
            }

            public void setPoiId(int poiId) {
                this.poiId = poiId;
            }

            public int getPreferential() {
                return preferential;
            }

            public void setPreferential(int preferential) {
                this.preferential = preferential;
            }

            public int getSellmin() {
                return sellmin;
            }

            public void setSellmin(int sellmin) {
                this.sellmin = sellmin;
            }

            public boolean isSell() {
                return sell;
            }

            public void setSell(boolean sell) {
                this.sell = sell;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getBrdId() {
                return brdId;
            }

            public void setBrdId(int brdId) {
                this.brdId = brdId;
            }

            public double getDealPrice() {
                return dealPrice;
            }

            public void setDealPrice(double dealPrice) {
                this.dealPrice = dealPrice;
            }

            public double getReferencePrice() {
                return referencePrice;
            }

            public void setReferencePrice(double referencePrice) {
                this.referencePrice = referencePrice;
            }

            public int getShowCount() {
                return showCount;
            }

            public void setShowCount(int showCount) {
                this.showCount = showCount;
            }
        }

        public static class 临安市Bean {
            /**
             * sellPrice : 30.0
             * area : 临安市
             * imax : 0
             * lat : 30.221964
             * lng : 119.7063
             * addr : 临安市锦城街道钱王街855号万华广场四楼（近锦城街）
             * brd : 比高电影城
             * dis : 临安
             * deal : 0
             * distance : 0
             * follow : 0
             * nm : 比高电影城(临安店)
             * ct :
             * poiId : 1438344
             * preferential : 0
             * sellmin : 0
             * sell : true
             * id : 519
             * brdId : 24525
             * dealPrice : 0.0
             * referencePrice : 35.0
             * showCount : 0
             */

            private double sellPrice;
            private String area;
            private int imax;
            private double lat;
            private double lng;
            private String addr;
            private String brd;
            private String dis;
            private int deal;
            private int distance;
            private int follow;
            private String nm;
            private String ct;
            private int poiId;
            private int preferential;
            private int sellmin;
            private boolean sell;
            private int id;
            private int brdId;
            private double dealPrice;
            private double referencePrice;
            private int showCount;

            public double getSellPrice() {
                return sellPrice;
            }

            public void setSellPrice(double sellPrice) {
                this.sellPrice = sellPrice;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public int getImax() {
                return imax;
            }

            public void setImax(int imax) {
                this.imax = imax;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public String getAddr() {
                return addr;
            }

            public void setAddr(String addr) {
                this.addr = addr;
            }

            public String getBrd() {
                return brd;
            }

            public void setBrd(String brd) {
                this.brd = brd;
            }

            public String getDis() {
                return dis;
            }

            public void setDis(String dis) {
                this.dis = dis;
            }

            public int getDeal() {
                return deal;
            }

            public void setDeal(int deal) {
                this.deal = deal;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public int getFollow() {
                return follow;
            }

            public void setFollow(int follow) {
                this.follow = follow;
            }

            public String getNm() {
                return nm;
            }

            public void setNm(String nm) {
                this.nm = nm;
            }

            public String getCt() {
                return ct;
            }

            public void setCt(String ct) {
                this.ct = ct;
            }

            public int getPoiId() {
                return poiId;
            }

            public void setPoiId(int poiId) {
                this.poiId = poiId;
            }

            public int getPreferential() {
                return preferential;
            }

            public void setPreferential(int preferential) {
                this.preferential = preferential;
            }

            public int getSellmin() {
                return sellmin;
            }

            public void setSellmin(int sellmin) {
                this.sellmin = sellmin;
            }

            public boolean isSell() {
                return sell;
            }

            public void setSell(boolean sell) {
                this.sell = sell;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getBrdId() {
                return brdId;
            }

            public void setBrdId(int brdId) {
                this.brdId = brdId;
            }

            public double getDealPrice() {
                return dealPrice;
            }

            public void setDealPrice(double dealPrice) {
                this.dealPrice = dealPrice;
            }

            public double getReferencePrice() {
                return referencePrice;
            }

            public void setReferencePrice(double referencePrice) {
                this.referencePrice = referencePrice;
            }

            public int getShowCount() {
                return showCount;
            }

            public void setShowCount(int showCount) {
                this.showCount = showCount;
            }
        }

        public static class 建德市Bean {
            /**
             * sellPrice : 27.0
             * area : 建德市
             * imax : 0
             * lat : 29.484692
             * lng : 119.29877
             * addr : 建德市严州大道988号金马中心裙楼3-4楼（衣之家百货3-4楼）
             * brd : 其它
             * dis : 建德
             * deal : 0
             * distance : 0
             * follow : 0
             * nm : 德纳国际影城(建德店)
             * ct :
             * poiId : 708222
             * preferential : 0
             * sellmin : 0
             * sell : true
             * id : 1796
             * brdId : 0
             * dealPrice : 0.0
             * referencePrice : 70.0
             * showCount : 0
             */

            private double sellPrice;
            private String area;
            private int imax;
            private double lat;
            private double lng;
            private String addr;
            private String brd;
            private String dis;
            private int deal;
            private int distance;
            private int follow;
            private String nm;
            private String ct;
            private int poiId;
            private int preferential;
            private int sellmin;
            private boolean sell;
            private int id;
            private int brdId;
            private double dealPrice;
            private double referencePrice;
            private int showCount;

            public double getSellPrice() {
                return sellPrice;
            }

            public void setSellPrice(double sellPrice) {
                this.sellPrice = sellPrice;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public int getImax() {
                return imax;
            }

            public void setImax(int imax) {
                this.imax = imax;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public String getAddr() {
                return addr;
            }

            public void setAddr(String addr) {
                this.addr = addr;
            }

            public String getBrd() {
                return brd;
            }

            public void setBrd(String brd) {
                this.brd = brd;
            }

            public String getDis() {
                return dis;
            }

            public void setDis(String dis) {
                this.dis = dis;
            }

            public int getDeal() {
                return deal;
            }

            public void setDeal(int deal) {
                this.deal = deal;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public int getFollow() {
                return follow;
            }

            public void setFollow(int follow) {
                this.follow = follow;
            }

            public String getNm() {
                return nm;
            }

            public void setNm(String nm) {
                this.nm = nm;
            }

            public String getCt() {
                return ct;
            }

            public void setCt(String ct) {
                this.ct = ct;
            }

            public int getPoiId() {
                return poiId;
            }

            public void setPoiId(int poiId) {
                this.poiId = poiId;
            }

            public int getPreferential() {
                return preferential;
            }

            public void setPreferential(int preferential) {
                this.preferential = preferential;
            }

            public int getSellmin() {
                return sellmin;
            }

            public void setSellmin(int sellmin) {
                this.sellmin = sellmin;
            }

            public boolean isSell() {
                return sell;
            }

            public void setSell(boolean sell) {
                this.sell = sell;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getBrdId() {
                return brdId;
            }

            public void setBrdId(int brdId) {
                this.brdId = brdId;
            }

            public double getDealPrice() {
                return dealPrice;
            }

            public void setDealPrice(double dealPrice) {
                this.dealPrice = dealPrice;
            }

            public double getReferencePrice() {
                return referencePrice;
            }

            public void setReferencePrice(double referencePrice) {
                this.referencePrice = referencePrice;
            }

            public int getShowCount() {
                return showCount;
            }

            public void setShowCount(int showCount) {
                this.showCount = showCount;
            }
        }

        public static class 淳安县Bean {
            /**
             * sellPrice : 23.0
             * area : 淳安县
             * imax : 0
             * lat : 29.598635
             * lng : 119.02757
             * addr : 淳安县千岛湖南景路渔歌府3-1、3-2号（鱼街3楼）
             * brd : 其它
             * dis :
             * deal : 0
             * distance : 0
             * follow : 0
             * nm : 汉鼎宇佑影城(千岛湖店)
             * ct :
             * poiId : 152314969
             * preferential : 0
             * sellmin : 0
             * sell : true
             * id : 17108
             * brdId : 0
             * dealPrice : 0.0
             * referencePrice : 0.0
             * showCount : 0
             */

            private double sellPrice;
            private String area;
            private int imax;
            private double lat;
            private double lng;
            private String addr;
            private String brd;
            private String dis;
            private int deal;
            private int distance;
            private int follow;
            private String nm;
            private String ct;
            private int poiId;
            private int preferential;
            private int sellmin;
            private boolean sell;
            private int id;
            private int brdId;
            private double dealPrice;
            private double referencePrice;
            private int showCount;

            public double getSellPrice() {
                return sellPrice;
            }

            public void setSellPrice(double sellPrice) {
                this.sellPrice = sellPrice;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public int getImax() {
                return imax;
            }

            public void setImax(int imax) {
                this.imax = imax;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public String getAddr() {
                return addr;
            }

            public void setAddr(String addr) {
                this.addr = addr;
            }

            public String getBrd() {
                return brd;
            }

            public void setBrd(String brd) {
                this.brd = brd;
            }

            public String getDis() {
                return dis;
            }

            public void setDis(String dis) {
                this.dis = dis;
            }

            public int getDeal() {
                return deal;
            }

            public void setDeal(int deal) {
                this.deal = deal;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }

            public int getFollow() {
                return follow;
            }

            public void setFollow(int follow) {
                this.follow = follow;
            }

            public String getNm() {
                return nm;
            }

            public void setNm(String nm) {
                this.nm = nm;
            }

            public String getCt() {
                return ct;
            }

            public void setCt(String ct) {
                this.ct = ct;
            }

            public int getPoiId() {
                return poiId;
            }

            public void setPoiId(int poiId) {
                this.poiId = poiId;
            }

            public int getPreferential() {
                return preferential;
            }

            public void setPreferential(int preferential) {
                this.preferential = preferential;
            }

            public int getSellmin() {
                return sellmin;
            }

            public void setSellmin(int sellmin) {
                this.sellmin = sellmin;
            }

            public boolean isSell() {
                return sell;
            }

            public void setSell(boolean sell) {
                this.sell = sell;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getBrdId() {
                return brdId;
            }

            public void setBrdId(int brdId) {
                this.brdId = brdId;
            }

            public double getDealPrice() {
                return dealPrice;
            }

            public void setDealPrice(double dealPrice) {
                this.dealPrice = dealPrice;
            }

            public double getReferencePrice() {
                return referencePrice;
            }

            public void setReferencePrice(double referencePrice) {
                this.referencePrice = referencePrice;
            }

            public int getShowCount() {
                return showCount;
            }

            public void setShowCount(int showCount) {
                this.showCount = showCount;
            }
        }
    }
}
