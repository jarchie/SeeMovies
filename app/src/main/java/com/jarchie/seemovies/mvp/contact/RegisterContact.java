package com.jarchie.seemovies.mvp.contact;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.widget.EditText;

import com.seemovies.sdk.mvp.base.BasePresenter;

/**
 * Created by Jarchie on 2017\11\17.
 * 描述：注册契约类
 */

public interface RegisterContact {
    interface presenter extends BasePresenter {
        void setHint(Context context, TextInputLayout userNameLayout, TextInputLayout passWordLayout);

        void register(Activity context, EditText editUserName, EditText editPwd);
    }
}
