package com.jarchie.seemovies.mvp.contact;

import android.content.Context;
import android.widget.TextView;

import com.seemovies.sdk.mvp.base.BasePresenter;

/**
 * Created by Jarchie on 2017\11\21.
 * 描述：欢迎页面的契约类
 */

public interface SplashContact {

    //处理倒计时跳转的业务逻辑
    interface presenter extends BasePresenter{
        //倒计时跳转
        void skipMainPage(Context context, TextView skipTv);

        void clickSkipMainPage(Context context,TextView skipTv);
    }

}
