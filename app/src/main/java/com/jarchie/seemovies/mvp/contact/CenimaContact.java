package com.jarchie.seemovies.mvp.contact;

import com.jarchie.seemovies.mvp.model.CenimaBean;
import com.seemovies.sdk.mvp.base.BasePresenter;
import com.seemovies.sdk.mvp.base.BaseView;

import java.util.List;

/**
 * Created by Jarchie on 2017\11\20.
 * 描述：影院契约类
 */

public interface CenimaContact {
    interface view extends BaseView {
        /**
         * 设置数据
         *
         * @param list
         */
        void setData(List<CenimaBean.DataBean.上城区Bean> list);
    }

    interface presenter extends BasePresenter {
        //获取数据
        void getData();
    }

}
