package com.jarchie.seemovies.mvp.contact;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.widget.CheckBox;
import android.widget.EditText;

import com.seemovies.sdk.mvp.base.BasePresenter;

/**
 * Created by Jarchie on 2017\11\17.
 * 描述：登录模块契约类
 */

public interface LoginContact {
    interface presenter extends BasePresenter {
        void initHint(Context context, TextInputLayout userNameLayout, TextInputLayout passWordLayout);

        void initSaveState(Context context, CheckBox mCheckBox, EditText editUserName, EditText editPwd);

        void login(Activity context,CheckBox mCheckBox, EditText editUserName, EditText editPwd);
    }
}
