package com.jarchie.seemovies.mvp.model;

import java.util.List;

/**
 * Created by Jarchie on 2017\11\9.
 * 描述：电影详情页实体类
 */

@SuppressWarnings("unused")
public class MovieDetailBean {

    /**
     * control : {"expires":3600}
     * status : 0
     * data : {"MovieDetailModel":{"cat":"剧情,悬疑,犯罪","dealsum":0,"dir":"肯尼思·布拉纳 ","dra":" 本片描述了发生在著名的\u201c东方快车\u201d上的一起离奇命案。大侦探波洛（肯尼思·布拉纳 饰）登上著名的东方快车，夜间三次被吵醒，第二天清晨便发现同车的美国富商雷切特（约翰尼·德普 饰）被人谋杀，死者被人戳了12刀，车上的各色人等都有嫌疑。波洛根据他所观察到的各种可疑迹象，以及对同车人的讯问，终于让案情大白于天下，而案件背后的往事也浮出水面。  ","dur":114,"id":344422,"imax":false,"img":"http://p1.meituan.net/165.220/movie/ce979de334b1b6229ba125b4a31f5ce1650096.png","isShowing":true,"late":false,"mk":0,"nm":"东方快车谋杀案","photos":["http://p0.meituan.net/w.h/mmdb/75732d2894d8353fd9aa98e64a65663f598137.jpg","http://p1.meituan.net/w.h/mmdb/c6a970eafdd24a829ab91fd5b3e5a0f1975743.jpg","http://p0.meituan.net/w.h/mmdb/a962f5166fb3caa5771e3f8d841c2607737708.jpg","http://p0.meituan.net/w.h/mmdb/6093bb0b905d600c8f3cc55b2ac3f5042061635.jpg","http://p0.meituan.net/w.h/mmdb/ed9574c714e389abc0898c7826b6a1da3840590.jpg","http://p0.meituan.net/w.h/mmdb/0fbf6bfa3975f6c9cc456fc666daf2df839865.jpg","http://p0.meituan.net/w.h/mmdb/289273eb5c4e2fcee7c431b8603af337774051.png","http://p0.meituan.net/w.h/mmdb/794830263bf21f254c60283fee54f3bd941034.png","http://p1.meituan.net/w.h/movie/baa900a98b0ff9df95341285c058f493202869.jpg","http://p1.meituan.net/w.h/movie/bdd31f2955a8b682091a0d4790bc853f172341.jpg","http://p0.meituan.net/w.h/movie/53654944effd18f00fc3e2421ff8e24c159622.jpg","http://p0.meituan.net/w.h/movie/e97e6039a889e99a40666ec5ee53a9f7117093.jpg","http://p0.meituan.net/w.h/movie/c17338e0f3826a688876ab923df19219156195.jpg","http://p1.meituan.net/w.h/movie/113633ca7fa59f9781a1003e6e344a26173013.jpg","http://p1.meituan.net/w.h/movie/37fd9d6d1f85a2511fab31a9d412b449142218.jpg","http://p0.meituan.net/w.h/movie/3403ea65070a545bc7df4be07b7b89d6172398.jpg","http://p0.meituan.net/w.h/movie/3dfb00052263188ed77f5157c20c4207183940.jpg","http://p0.meituan.net/w.h/mmdb/8b5a970c9af292472dbf0cd1ed6c3afe3789204.png","http://p1.meituan.net/w.h/mmdb/291b45d82bbb597d04f60ed9f6b3536d135237.jpg","http://p1.meituan.net/w.h/mmdb/a0d5fb304a964292cbf1584287f29fc5142071.jpg"],"pn":108,"preSale":0,"rt":"2017-11-10上映","sc":8.2,"scm":"","showSnum":true,"snum":36585,"src":"美国","star":"肯尼思·布拉纳 刘风 约翰尼·德普 王千源 米歇尔·菲佛 俞飞鸿 黛茜·雷德利 朱迪·丹奇 曹雷 露西·宝通 德里克·雅各比 莱斯利·奥多姆 汤姆·巴特曼 乔什·盖德 马尔万·肯扎里 佩内洛普·克鲁兹 威廉·达福 奥莉薇娅·柯尔曼 谢尔盖·普诺宁 曼努埃尔·加西亚-鲁尔福 米兰达·莱森 Jason Matthewson Bern Collaco Tomasz Dabrowski 托尼·保罗·韦斯特 Tate Pitchie-Cooper Dardan Kolicaj Hayat Kamille Nick Owenford Angela Holmes Alan Calton Gerald Maliqi Lampros Kalfuntzos Kate Tydman 亚当·加西亚 Alaa Safi 约瑟夫·朗 拉斯科·阿特金斯 齐亚德·阿巴扎 Jill Buchanan Bernardo Santos Asan N'Jie James Pimenta Ross Carter 安吉丽娜·朱莉 ","vd":"http://maoyan.meituan.net/movie/videos/854x4807e7b48650fad4337b3207ae08e7eee7b.mp4","ver":"2D","vnum":18,"wish":71604,"wishst":0},"CommentResponseModel":{"cmts":[{"nick":"付冬100","approve":0,"oppose":0,"reply":0,"avatarurl":"https://img.meituan.net/avatar/4773a4ecf10a47e30f34b4950ca748b5106237.jpg","nickName":"付冬100","score":5,"userId":75258634,"vipInfo":"","id":128838594,"content":"一车人谋杀一个人","time":"2017-11-13 14:03"},{"nick":"RxC758660659","approve":0,"oppose":0,"reply":0,"avatarurl":"","nickName":"RxC758660659","score":5,"userId":1148550429,"vipInfo":"","id":128838590,"content":"非常好的结局 但不能说完美","time":"2017-11-13 14:03"},{"nick":"zhaojin8305","approve":0,"oppose":0,"reply":0,"avatarurl":"https://img.meituan.net/avatar/e2997b5c7a04a7f06834f919e1fd46fd309200.jpg","nickName":"zhaojin8305","score":3.5,"userId":2350379,"vipInfo":"","id":128838585,"content":"是一部翻拍片..主演缺少了之前的幽默感...大碗很多..情结没有特别突出的亮点，结局没新意，毕竟是部翻拍片","time":"2017-11-13 14:03"},{"nick":"安静高兴","approve":0,"oppose":0,"reply":0,"avatarurl":"https://img.meituan.net/avatar/c7b6165d7d8653304ca21876548b084110254.jpg","nickName":"安静高兴","score":0.5,"userId":672248721,"vipInfo":"","id":128838555,"content":"糊弄鬼呢，十个人一列火车场景，把一部电影拍完了，更搞笑的是，最后结局所有人都是凶手，至于么为了杀一个渣滓各种有身份地位的人全参与了，简直无语。8.1分怎么刷的呢，迄今为止看过的最烂的一部电影。","time":"2017-11-13 14:02"},{"nick":"繇繇公主","approve":0,"oppose":0,"reply":0,"avatarurl":"","nickName":"繇繇公主","score":4,"userId":76395729,"vipInfo":"","id":128838537,"content":"很好的一部影片，画面感很好，思维逻辑很强，能跟上节拍，很合众.希望大家不要错过！","time":"2017-11-13 14:02"}],"hcmts":[{"nick":"tTS530336450","approve":533,"oppose":0,"reply":28,"avatarurl":"","nickName":"tTS530336450","score":5,"userId":215895676,"vipInfo":"","id":128581795,"content":"第一次写电影的影评，送给东方快车谋杀案。\n作为一个没有看过原作的观众，这部电影完美向我展示了这么一个严谨而又超乎寻常的几乎完美谋杀案，如果没有那么一场雪崩（当然没有如果）给10分，并非口味挑剔的专业观众或者领钱水军，一部电影能给我带来欣喜，惊叹，感动，反思，那么它就值得我的分数","time":"2017-11-10 18:12"},{"nick":"CuK361127384","approve":344,"oppose":0,"reply":5,"avatarurl":"https://img.meituan.net/avatar/f8c2c303a143e09565b28b5810a3bc6b119696.jpg","nickName":"CuK361127384","score":4.5,"userId":325946759,"vipInfo":"","id":128581799,"content":"经典重塑，悬疑中有真情，真情中有未知，未知中有破碎，破碎中有羁绊，值得一看。","time":"2017-11-10 18:12"},{"nick":"鸠鸣笛迦","approve":351,"oppose":0,"reply":7,"avatarurl":"https://img.meituan.net/avatar/b5118c9b8d67edb4b971a7f189bb5cbf57099.jpg","nickName":"鸠鸣笛迦","score":4.5,"userId":111049279,"vipInfo":"","id":128581884,"content":"没看过原著和相关电影，对这部剧一片空白的进去\u2014\u2014坚持正义的侦探，各有隐情的乘客，陈年的杀人事件，在列车上相遇，当事情的真相一点点浮现，当最后的结局一点点展开，正义的天平开始失衡，世界并非黑白两色，最后的最后却正是人性的选择","time":"2017-11-10 18:13"},{"nick":"无敌西瓜超人","approve":61,"oppose":0,"reply":8,"avatarurl":"https://img.meituan.net/avatar/b8e27ba62a6c108cc675b52078c99281150073.jpg","nickName":"无敌西瓜超人","score":3.5,"userId":24923740,"vipInfo":"","id":128559558,"content":"作為一部偵探片還是中規中矩，故事講得很清晰，在作案動機上每個乘客都被探長剖析了出來，影院的杜比音響效果很震撼，期待續集能更精彩~","time":"2017-11-10 13:48"},{"nick":"玉米到春天","approve":137,"oppose":0,"reply":8,"avatarurl":"https://img.meituan.net/avatar/d4edccb850d8e766d40e26d5f15880c850470.jpg","nickName":"豆丁乖","score":3.5,"userId":262360895,"vipInfo":"","id":128545341,"content":"东方快车谋杀案，在一整部电影里，演员可以说是最亮眼的地方了。由于剧情的原因，每个角色在故事推进的过程中，都表现出了不同类型不同程度的悲恸。但每个人的表现方式都是不同的，但又各自切合角色。电影里很多画面构图都很美，尤其是剧情发展到后来，波洛穿过纷飞的大雪走向长桌，桌边人姿态表情各异，仿佛是在致敬最后的晚餐。因为原作实在是太经典了，以至于大家都知道这个故事是怎么回事，依然要去电影院观看！","time":"2017-11-10 10:34"},{"nick":"_qqn6h1438424789","approve":214,"oppose":0,"reply":9,"avatarurl":"https://img.meituan.net/avatar/b096b1b63bed0851d8115a4048246aad10613.jpg","nickName":"_qqn6h1438424789","score":5,"userId":239988954,"vipInfo":"","id":128270116,"content":"给我阿婆疯狂打call啊 真的是非常好的故事 看完书浑身鸡皮疙瘩都起来了","time":"2017-11-05 21:31"},{"nick":"德基广场","approve":161,"oppose":0,"reply":2,"avatarurl":"https://img.meituan.net/avatar/a0b700a8dd3b5d39b6012e15c4a0dac333160.jpg","nickName":"德基广场","score":5,"userId":6262637,"vipInfo":"","id":128276481,"content":"书相当好看！老版的电影也不错，那个胖胖的波洛大叔！这个大叔还帅点，期待啦！","time":"2017-11-05 22:25"},{"nick":"些许暖意","approve":162,"oppose":0,"reply":28,"avatarurl":"https://img.meituan.net/avatar/be1dc5b3474095c85beb3310652169ed56355.jpg","nickName":"些许暖意","score":0,"userId":45559477,"vipInfo":"","id":128291557,"content":"阿加莎关于波罗的故事我最喜欢的还是《阳光下的罪恶》和《尼罗河上的惨案》，对《东方快车谋杀案》很无感，但是冲着阿婆，也得去捧个场，这是阿婆的作品第一次登上国内大银幕呢😄","time":"2017-11-06 08:20"},{"nick":"banbi白","approve":71,"oppose":0,"reply":45,"avatarurl":"https://img.meituan.net/avatar/968c4a275fedf5e23b94e8fceeda3e5d262013.jpg","nickName":"banbi白","score":4,"userId":104846575,"vipInfo":"","id":128538114,"content":"嗯\u2026看过了消失的客人后，觉得套路差不多！某些自以为看过几本书的人不要和我bb没完了！就这部电影来说我个人的确更喜欢消失的客人！那些看过原著的别来和我显摆个没玩！这部电影我也给了八分！","time":"2017-11-10 15:38"},{"nick":"dorashiwo","approve":47,"oppose":0,"reply":6,"avatarurl":"https://img.meituan.net/avatar/7395fc7832028e31d52326d257800b4a12750.jpg","nickName":"dorashiwo","score":3.5,"userId":92232998,"vipInfo":"","id":128539007,"content":"不爱这个结局啊。还是喜欢出人意料反转再反转的凶手，比较刺激。这个结果，不大喜欢。不过影片拍的不错，很多画面挺美的，而且一镜到底真的很6。","time":"2017-11-10 04:27"}],"total":14001,"hasNext":true}}
     */

    private ControlBean control;
    private int status;
    private DataBean data;

    public ControlBean getControl() {
        return control;
    }

    public void setControl(ControlBean control) {
        this.control = control;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ControlBean {
        /**
         * expires : 3600
         */

        private int expires;

        public int getExpires() {
            return expires;
        }

        public void setExpires(int expires) {
            this.expires = expires;
        }
    }

    public static class DataBean {
        /**
         * MovieDetailModel : {"cat":"剧情,悬疑,犯罪","dealsum":0,"dir":"肯尼思·布拉纳 ","dra":" 本片描述了发生在著名的\u201c东方快车\u201d上的一起离奇命案。大侦探波洛（肯尼思·布拉纳 饰）登上著名的东方快车，夜间三次被吵醒，第二天清晨便发现同车的美国富商雷切特（约翰尼·德普 饰）被人谋杀，死者被人戳了12刀，车上的各色人等都有嫌疑。波洛根据他所观察到的各种可疑迹象，以及对同车人的讯问，终于让案情大白于天下，而案件背后的往事也浮出水面。  ","dur":114,"id":344422,"imax":false,"img":"http://p1.meituan.net/165.220/movie/ce979de334b1b6229ba125b4a31f5ce1650096.png","isShowing":true,"late":false,"mk":0,"nm":"东方快车谋杀案","photos":["http://p0.meituan.net/w.h/mmdb/75732d2894d8353fd9aa98e64a65663f598137.jpg","http://p1.meituan.net/w.h/mmdb/c6a970eafdd24a829ab91fd5b3e5a0f1975743.jpg","http://p0.meituan.net/w.h/mmdb/a962f5166fb3caa5771e3f8d841c2607737708.jpg","http://p0.meituan.net/w.h/mmdb/6093bb0b905d600c8f3cc55b2ac3f5042061635.jpg","http://p0.meituan.net/w.h/mmdb/ed9574c714e389abc0898c7826b6a1da3840590.jpg","http://p0.meituan.net/w.h/mmdb/0fbf6bfa3975f6c9cc456fc666daf2df839865.jpg","http://p0.meituan.net/w.h/mmdb/289273eb5c4e2fcee7c431b8603af337774051.png","http://p0.meituan.net/w.h/mmdb/794830263bf21f254c60283fee54f3bd941034.png","http://p1.meituan.net/w.h/movie/baa900a98b0ff9df95341285c058f493202869.jpg","http://p1.meituan.net/w.h/movie/bdd31f2955a8b682091a0d4790bc853f172341.jpg","http://p0.meituan.net/w.h/movie/53654944effd18f00fc3e2421ff8e24c159622.jpg","http://p0.meituan.net/w.h/movie/e97e6039a889e99a40666ec5ee53a9f7117093.jpg","http://p0.meituan.net/w.h/movie/c17338e0f3826a688876ab923df19219156195.jpg","http://p1.meituan.net/w.h/movie/113633ca7fa59f9781a1003e6e344a26173013.jpg","http://p1.meituan.net/w.h/movie/37fd9d6d1f85a2511fab31a9d412b449142218.jpg","http://p0.meituan.net/w.h/movie/3403ea65070a545bc7df4be07b7b89d6172398.jpg","http://p0.meituan.net/w.h/movie/3dfb00052263188ed77f5157c20c4207183940.jpg","http://p0.meituan.net/w.h/mmdb/8b5a970c9af292472dbf0cd1ed6c3afe3789204.png","http://p1.meituan.net/w.h/mmdb/291b45d82bbb597d04f60ed9f6b3536d135237.jpg","http://p1.meituan.net/w.h/mmdb/a0d5fb304a964292cbf1584287f29fc5142071.jpg"],"pn":108,"preSale":0,"rt":"2017-11-10上映","sc":8.2,"scm":"","showSnum":true,"snum":36585,"src":"美国","star":"肯尼思·布拉纳 刘风 约翰尼·德普 王千源 米歇尔·菲佛 俞飞鸿 黛茜·雷德利 朱迪·丹奇 曹雷 露西·宝通 德里克·雅各比 莱斯利·奥多姆 汤姆·巴特曼 乔什·盖德 马尔万·肯扎里 佩内洛普·克鲁兹 威廉·达福 奥莉薇娅·柯尔曼 谢尔盖·普诺宁 曼努埃尔·加西亚-鲁尔福 米兰达·莱森 Jason Matthewson Bern Collaco Tomasz Dabrowski 托尼·保罗·韦斯特 Tate Pitchie-Cooper Dardan Kolicaj Hayat Kamille Nick Owenford Angela Holmes Alan Calton Gerald Maliqi Lampros Kalfuntzos Kate Tydman 亚当·加西亚 Alaa Safi 约瑟夫·朗 拉斯科·阿特金斯 齐亚德·阿巴扎 Jill Buchanan Bernardo Santos Asan N'Jie James Pimenta Ross Carter 安吉丽娜·朱莉 ","vd":"http://maoyan.meituan.net/movie/videos/854x4807e7b48650fad4337b3207ae08e7eee7b.mp4","ver":"2D","vnum":18,"wish":71604,"wishst":0}
         * CommentResponseModel : {"cmts":[{"nick":"付冬100","approve":0,"oppose":0,"reply":0,"avatarurl":"https://img.meituan.net/avatar/4773a4ecf10a47e30f34b4950ca748b5106237.jpg","nickName":"付冬100","score":5,"userId":75258634,"vipInfo":"","id":128838594,"content":"一车人谋杀一个人","time":"2017-11-13 14:03"},{"nick":"RxC758660659","approve":0,"oppose":0,"reply":0,"avatarurl":"","nickName":"RxC758660659","score":5,"userId":1148550429,"vipInfo":"","id":128838590,"content":"非常好的结局 但不能说完美","time":"2017-11-13 14:03"},{"nick":"zhaojin8305","approve":0,"oppose":0,"reply":0,"avatarurl":"https://img.meituan.net/avatar/e2997b5c7a04a7f06834f919e1fd46fd309200.jpg","nickName":"zhaojin8305","score":3.5,"userId":2350379,"vipInfo":"","id":128838585,"content":"是一部翻拍片..主演缺少了之前的幽默感...大碗很多..情结没有特别突出的亮点，结局没新意，毕竟是部翻拍片","time":"2017-11-13 14:03"},{"nick":"安静高兴","approve":0,"oppose":0,"reply":0,"avatarurl":"https://img.meituan.net/avatar/c7b6165d7d8653304ca21876548b084110254.jpg","nickName":"安静高兴","score":0.5,"userId":672248721,"vipInfo":"","id":128838555,"content":"糊弄鬼呢，十个人一列火车场景，把一部电影拍完了，更搞笑的是，最后结局所有人都是凶手，至于么为了杀一个渣滓各种有身份地位的人全参与了，简直无语。8.1分怎么刷的呢，迄今为止看过的最烂的一部电影。","time":"2017-11-13 14:02"},{"nick":"繇繇公主","approve":0,"oppose":0,"reply":0,"avatarurl":"","nickName":"繇繇公主","score":4,"userId":76395729,"vipInfo":"","id":128838537,"content":"很好的一部影片，画面感很好，思维逻辑很强，能跟上节拍，很合众.希望大家不要错过！","time":"2017-11-13 14:02"}],"hcmts":[{"nick":"tTS530336450","approve":533,"oppose":0,"reply":28,"avatarurl":"","nickName":"tTS530336450","score":5,"userId":215895676,"vipInfo":"","id":128581795,"content":"第一次写电影的影评，送给东方快车谋杀案。\n作为一个没有看过原作的观众，这部电影完美向我展示了这么一个严谨而又超乎寻常的几乎完美谋杀案，如果没有那么一场雪崩（当然没有如果）给10分，并非口味挑剔的专业观众或者领钱水军，一部电影能给我带来欣喜，惊叹，感动，反思，那么它就值得我的分数","time":"2017-11-10 18:12"},{"nick":"CuK361127384","approve":344,"oppose":0,"reply":5,"avatarurl":"https://img.meituan.net/avatar/f8c2c303a143e09565b28b5810a3bc6b119696.jpg","nickName":"CuK361127384","score":4.5,"userId":325946759,"vipInfo":"","id":128581799,"content":"经典重塑，悬疑中有真情，真情中有未知，未知中有破碎，破碎中有羁绊，值得一看。","time":"2017-11-10 18:12"},{"nick":"鸠鸣笛迦","approve":351,"oppose":0,"reply":7,"avatarurl":"https://img.meituan.net/avatar/b5118c9b8d67edb4b971a7f189bb5cbf57099.jpg","nickName":"鸠鸣笛迦","score":4.5,"userId":111049279,"vipInfo":"","id":128581884,"content":"没看过原著和相关电影，对这部剧一片空白的进去\u2014\u2014坚持正义的侦探，各有隐情的乘客，陈年的杀人事件，在列车上相遇，当事情的真相一点点浮现，当最后的结局一点点展开，正义的天平开始失衡，世界并非黑白两色，最后的最后却正是人性的选择","time":"2017-11-10 18:13"},{"nick":"无敌西瓜超人","approve":61,"oppose":0,"reply":8,"avatarurl":"https://img.meituan.net/avatar/b8e27ba62a6c108cc675b52078c99281150073.jpg","nickName":"无敌西瓜超人","score":3.5,"userId":24923740,"vipInfo":"","id":128559558,"content":"作為一部偵探片還是中規中矩，故事講得很清晰，在作案動機上每個乘客都被探長剖析了出來，影院的杜比音響效果很震撼，期待續集能更精彩~","time":"2017-11-10 13:48"},{"nick":"玉米到春天","approve":137,"oppose":0,"reply":8,"avatarurl":"https://img.meituan.net/avatar/d4edccb850d8e766d40e26d5f15880c850470.jpg","nickName":"豆丁乖","score":3.5,"userId":262360895,"vipInfo":"","id":128545341,"content":"东方快车谋杀案，在一整部电影里，演员可以说是最亮眼的地方了。由于剧情的原因，每个角色在故事推进的过程中，都表现出了不同类型不同程度的悲恸。但每个人的表现方式都是不同的，但又各自切合角色。电影里很多画面构图都很美，尤其是剧情发展到后来，波洛穿过纷飞的大雪走向长桌，桌边人姿态表情各异，仿佛是在致敬最后的晚餐。因为原作实在是太经典了，以至于大家都知道这个故事是怎么回事，依然要去电影院观看！","time":"2017-11-10 10:34"},{"nick":"_qqn6h1438424789","approve":214,"oppose":0,"reply":9,"avatarurl":"https://img.meituan.net/avatar/b096b1b63bed0851d8115a4048246aad10613.jpg","nickName":"_qqn6h1438424789","score":5,"userId":239988954,"vipInfo":"","id":128270116,"content":"给我阿婆疯狂打call啊 真的是非常好的故事 看完书浑身鸡皮疙瘩都起来了","time":"2017-11-05 21:31"},{"nick":"德基广场","approve":161,"oppose":0,"reply":2,"avatarurl":"https://img.meituan.net/avatar/a0b700a8dd3b5d39b6012e15c4a0dac333160.jpg","nickName":"德基广场","score":5,"userId":6262637,"vipInfo":"","id":128276481,"content":"书相当好看！老版的电影也不错，那个胖胖的波洛大叔！这个大叔还帅点，期待啦！","time":"2017-11-05 22:25"},{"nick":"些许暖意","approve":162,"oppose":0,"reply":28,"avatarurl":"https://img.meituan.net/avatar/be1dc5b3474095c85beb3310652169ed56355.jpg","nickName":"些许暖意","score":0,"userId":45559477,"vipInfo":"","id":128291557,"content":"阿加莎关于波罗的故事我最喜欢的还是《阳光下的罪恶》和《尼罗河上的惨案》，对《东方快车谋杀案》很无感，但是冲着阿婆，也得去捧个场，这是阿婆的作品第一次登上国内大银幕呢😄","time":"2017-11-06 08:20"},{"nick":"banbi白","approve":71,"oppose":0,"reply":45,"avatarurl":"https://img.meituan.net/avatar/968c4a275fedf5e23b94e8fceeda3e5d262013.jpg","nickName":"banbi白","score":4,"userId":104846575,"vipInfo":"","id":128538114,"content":"嗯\u2026看过了消失的客人后，觉得套路差不多！某些自以为看过几本书的人不要和我bb没完了！就这部电影来说我个人的确更喜欢消失的客人！那些看过原著的别来和我显摆个没玩！这部电影我也给了八分！","time":"2017-11-10 15:38"},{"nick":"dorashiwo","approve":47,"oppose":0,"reply":6,"avatarurl":"https://img.meituan.net/avatar/7395fc7832028e31d52326d257800b4a12750.jpg","nickName":"dorashiwo","score":3.5,"userId":92232998,"vipInfo":"","id":128539007,"content":"不爱这个结局啊。还是喜欢出人意料反转再反转的凶手，比较刺激。这个结果，不大喜欢。不过影片拍的不错，很多画面挺美的，而且一镜到底真的很6。","time":"2017-11-10 04:27"}],"total":14001,"hasNext":true}
         */

        private MovieDetailModelBean MovieDetailModel;
        private CommentResponseModelBean CommentResponseModel;

        public MovieDetailModelBean getMovieDetailModel() {
            return MovieDetailModel;
        }

        public void setMovieDetailModel(MovieDetailModelBean MovieDetailModel) {
            this.MovieDetailModel = MovieDetailModel;
        }

        public CommentResponseModelBean getCommentResponseModel() {
            return CommentResponseModel;
        }

        public void setCommentResponseModel(CommentResponseModelBean CommentResponseModel) {
            this.CommentResponseModel = CommentResponseModel;
        }

        public static class MovieDetailModelBean {
            /**
             * cat : 剧情,悬疑,犯罪
             * dealsum : 0
             * dir : 肯尼思·布拉纳
             * dra :  本片描述了发生在著名的“东方快车”上的一起离奇命案。大侦探波洛（肯尼思·布拉纳 饰）登上著名的东方快车，夜间三次被吵醒，第二天清晨便发现同车的美国富商雷切特（约翰尼·德普 饰）被人谋杀，死者被人戳了12刀，车上的各色人等都有嫌疑。波洛根据他所观察到的各种可疑迹象，以及对同车人的讯问，终于让案情大白于天下，而案件背后的往事也浮出水面。
             * dur : 114
             * id : 344422
             * imax : false
             * img : http://p1.meituan.net/165.220/movie/ce979de334b1b6229ba125b4a31f5ce1650096.png
             * isShowing : true
             * late : false
             * mk : 0
             * nm : 东方快车谋杀案
             * photos : ["http://p0.meituan.net/w.h/mmdb/75732d2894d8353fd9aa98e64a65663f598137.jpg","http://p1.meituan.net/w.h/mmdb/c6a970eafdd24a829ab91fd5b3e5a0f1975743.jpg","http://p0.meituan.net/w.h/mmdb/a962f5166fb3caa5771e3f8d841c2607737708.jpg","http://p0.meituan.net/w.h/mmdb/6093bb0b905d600c8f3cc55b2ac3f5042061635.jpg","http://p0.meituan.net/w.h/mmdb/ed9574c714e389abc0898c7826b6a1da3840590.jpg","http://p0.meituan.net/w.h/mmdb/0fbf6bfa3975f6c9cc456fc666daf2df839865.jpg","http://p0.meituan.net/w.h/mmdb/289273eb5c4e2fcee7c431b8603af337774051.png","http://p0.meituan.net/w.h/mmdb/794830263bf21f254c60283fee54f3bd941034.png","http://p1.meituan.net/w.h/movie/baa900a98b0ff9df95341285c058f493202869.jpg","http://p1.meituan.net/w.h/movie/bdd31f2955a8b682091a0d4790bc853f172341.jpg","http://p0.meituan.net/w.h/movie/53654944effd18f00fc3e2421ff8e24c159622.jpg","http://p0.meituan.net/w.h/movie/e97e6039a889e99a40666ec5ee53a9f7117093.jpg","http://p0.meituan.net/w.h/movie/c17338e0f3826a688876ab923df19219156195.jpg","http://p1.meituan.net/w.h/movie/113633ca7fa59f9781a1003e6e344a26173013.jpg","http://p1.meituan.net/w.h/movie/37fd9d6d1f85a2511fab31a9d412b449142218.jpg","http://p0.meituan.net/w.h/movie/3403ea65070a545bc7df4be07b7b89d6172398.jpg","http://p0.meituan.net/w.h/movie/3dfb00052263188ed77f5157c20c4207183940.jpg","http://p0.meituan.net/w.h/mmdb/8b5a970c9af292472dbf0cd1ed6c3afe3789204.png","http://p1.meituan.net/w.h/mmdb/291b45d82bbb597d04f60ed9f6b3536d135237.jpg","http://p1.meituan.net/w.h/mmdb/a0d5fb304a964292cbf1584287f29fc5142071.jpg"]
             * pn : 108
             * preSale : 0
             * rt : 2017-11-10上映
             * sc : 8.2
             * scm :
             * showSnum : true
             * snum : 36585
             * src : 美国
             * star : 肯尼思·布拉纳 刘风 约翰尼·德普 王千源 米歇尔·菲佛 俞飞鸿 黛茜·雷德利 朱迪·丹奇 曹雷 露西·宝通 德里克·雅各比 莱斯利·奥多姆 汤姆·巴特曼 乔什·盖德 马尔万·肯扎里 佩内洛普·克鲁兹 威廉·达福 奥莉薇娅·柯尔曼 谢尔盖·普诺宁 曼努埃尔·加西亚-鲁尔福 米兰达·莱森 Jason Matthewson Bern Collaco Tomasz Dabrowski 托尼·保罗·韦斯特 Tate Pitchie-Cooper Dardan Kolicaj Hayat Kamille Nick Owenford Angela Holmes Alan Calton Gerald Maliqi Lampros Kalfuntzos Kate Tydman 亚当·加西亚 Alaa Safi 约瑟夫·朗 拉斯科·阿特金斯 齐亚德·阿巴扎 Jill Buchanan Bernardo Santos Asan N'Jie James Pimenta Ross Carter 安吉丽娜·朱莉
             * vd : http://maoyan.meituan.net/movie/videos/854x4807e7b48650fad4337b3207ae08e7eee7b.mp4
             * ver : 2D
             * vnum : 18
             * wish : 71604
             * wishst : 0
             */

            private String cat;
            private int dealsum;
            private String dir;
            private String dra;
            private int dur;
            private int id;
            private boolean imax;
            private String img;
            private boolean isShowing;
            private boolean late;
            private int mk;
            private String nm;
            private int pn;
            private int preSale;
            private String rt;
            private float sc;
            private String scm;
            private boolean showSnum;
            private int snum;
            private String src;
            private String star;
            private String vd;
            private String ver;
            private int vnum;
            private int wish;
            private int wishst;
            private List<String> photos;

            public String getCat() {
                return cat;
            }

            public void setCat(String cat) {
                this.cat = cat;
            }

            public int getDealsum() {
                return dealsum;
            }

            public void setDealsum(int dealsum) {
                this.dealsum = dealsum;
            }

            public String getDir() {
                return dir;
            }

            public void setDir(String dir) {
                this.dir = dir;
            }

            public String getDra() {
                return dra;
            }

            public void setDra(String dra) {
                this.dra = dra;
            }

            public int getDur() {
                return dur;
            }

            public void setDur(int dur) {
                this.dur = dur;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public boolean isImax() {
                return imax;
            }

            public void setImax(boolean imax) {
                this.imax = imax;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public boolean isIsShowing() {
                return isShowing;
            }

            public void setIsShowing(boolean isShowing) {
                this.isShowing = isShowing;
            }

            public boolean isLate() {
                return late;
            }

            public void setLate(boolean late) {
                this.late = late;
            }

            public int getMk() {
                return mk;
            }

            public void setMk(int mk) {
                this.mk = mk;
            }

            public String getNm() {
                return nm;
            }

            public void setNm(String nm) {
                this.nm = nm;
            }

            public int getPn() {
                return pn;
            }

            public void setPn(int pn) {
                this.pn = pn;
            }

            public int getPreSale() {
                return preSale;
            }

            public void setPreSale(int preSale) {
                this.preSale = preSale;
            }

            public String getRt() {
                return rt;
            }

            public void setRt(String rt) {
                this.rt = rt;
            }

            public float getSc() {
                return sc;
            }

            public void setSc(float sc) {
                this.sc = sc;
            }

            public String getScm() {
                return scm;
            }

            public void setScm(String scm) {
                this.scm = scm;
            }

            public boolean isShowSnum() {
                return showSnum;
            }

            public void setShowSnum(boolean showSnum) {
                this.showSnum = showSnum;
            }

            public int getSnum() {
                return snum;
            }

            public void setSnum(int snum) {
                this.snum = snum;
            }

            public String getSrc() {
                return src;
            }

            public void setSrc(String src) {
                this.src = src;
            }

            public String getStar() {
                return star;
            }

            public void setStar(String star) {
                this.star = star;
            }

            public String getVd() {
                return vd;
            }

            public void setVd(String vd) {
                this.vd = vd;
            }

            public String getVer() {
                return ver;
            }

            public void setVer(String ver) {
                this.ver = ver;
            }

            public int getVnum() {
                return vnum;
            }

            public void setVnum(int vnum) {
                this.vnum = vnum;
            }

            public int getWish() {
                return wish;
            }

            public void setWish(int wish) {
                this.wish = wish;
            }

            public int getWishst() {
                return wishst;
            }

            public void setWishst(int wishst) {
                this.wishst = wishst;
            }

            public List<String> getPhotos() {
                return photos;
            }

            public void setPhotos(List<String> photos) {
                this.photos = photos;
            }
        }

        public static class CommentResponseModelBean {
            /**
             * cmts : [{"nick":"付冬100","approve":0,"oppose":0,"reply":0,"avatarurl":"https://img.meituan.net/avatar/4773a4ecf10a47e30f34b4950ca748b5106237.jpg","nickName":"付冬100","score":5,"userId":75258634,"vipInfo":"","id":128838594,"content":"一车人谋杀一个人","time":"2017-11-13 14:03"},{"nick":"RxC758660659","approve":0,"oppose":0,"reply":0,"avatarurl":"","nickName":"RxC758660659","score":5,"userId":1148550429,"vipInfo":"","id":128838590,"content":"非常好的结局 但不能说完美","time":"2017-11-13 14:03"},{"nick":"zhaojin8305","approve":0,"oppose":0,"reply":0,"avatarurl":"https://img.meituan.net/avatar/e2997b5c7a04a7f06834f919e1fd46fd309200.jpg","nickName":"zhaojin8305","score":3.5,"userId":2350379,"vipInfo":"","id":128838585,"content":"是一部翻拍片..主演缺少了之前的幽默感...大碗很多..情结没有特别突出的亮点，结局没新意，毕竟是部翻拍片","time":"2017-11-13 14:03"},{"nick":"安静高兴","approve":0,"oppose":0,"reply":0,"avatarurl":"https://img.meituan.net/avatar/c7b6165d7d8653304ca21876548b084110254.jpg","nickName":"安静高兴","score":0.5,"userId":672248721,"vipInfo":"","id":128838555,"content":"糊弄鬼呢，十个人一列火车场景，把一部电影拍完了，更搞笑的是，最后结局所有人都是凶手，至于么为了杀一个渣滓各种有身份地位的人全参与了，简直无语。8.1分怎么刷的呢，迄今为止看过的最烂的一部电影。","time":"2017-11-13 14:02"},{"nick":"繇繇公主","approve":0,"oppose":0,"reply":0,"avatarurl":"","nickName":"繇繇公主","score":4,"userId":76395729,"vipInfo":"","id":128838537,"content":"很好的一部影片，画面感很好，思维逻辑很强，能跟上节拍，很合众.希望大家不要错过！","time":"2017-11-13 14:02"}]
             * hcmts : [{"nick":"tTS530336450","approve":533,"oppose":0,"reply":28,"avatarurl":"","nickName":"tTS530336450","score":5,"userId":215895676,"vipInfo":"","id":128581795,"content":"第一次写电影的影评，送给东方快车谋杀案。\n作为一个没有看过原作的观众，这部电影完美向我展示了这么一个严谨而又超乎寻常的几乎完美谋杀案，如果没有那么一场雪崩（当然没有如果）给10分，并非口味挑剔的专业观众或者领钱水军，一部电影能给我带来欣喜，惊叹，感动，反思，那么它就值得我的分数","time":"2017-11-10 18:12"},{"nick":"CuK361127384","approve":344,"oppose":0,"reply":5,"avatarurl":"https://img.meituan.net/avatar/f8c2c303a143e09565b28b5810a3bc6b119696.jpg","nickName":"CuK361127384","score":4.5,"userId":325946759,"vipInfo":"","id":128581799,"content":"经典重塑，悬疑中有真情，真情中有未知，未知中有破碎，破碎中有羁绊，值得一看。","time":"2017-11-10 18:12"},{"nick":"鸠鸣笛迦","approve":351,"oppose":0,"reply":7,"avatarurl":"https://img.meituan.net/avatar/b5118c9b8d67edb4b971a7f189bb5cbf57099.jpg","nickName":"鸠鸣笛迦","score":4.5,"userId":111049279,"vipInfo":"","id":128581884,"content":"没看过原著和相关电影，对这部剧一片空白的进去\u2014\u2014坚持正义的侦探，各有隐情的乘客，陈年的杀人事件，在列车上相遇，当事情的真相一点点浮现，当最后的结局一点点展开，正义的天平开始失衡，世界并非黑白两色，最后的最后却正是人性的选择","time":"2017-11-10 18:13"},{"nick":"无敌西瓜超人","approve":61,"oppose":0,"reply":8,"avatarurl":"https://img.meituan.net/avatar/b8e27ba62a6c108cc675b52078c99281150073.jpg","nickName":"无敌西瓜超人","score":3.5,"userId":24923740,"vipInfo":"","id":128559558,"content":"作為一部偵探片還是中規中矩，故事講得很清晰，在作案動機上每個乘客都被探長剖析了出來，影院的杜比音響效果很震撼，期待續集能更精彩~","time":"2017-11-10 13:48"},{"nick":"玉米到春天","approve":137,"oppose":0,"reply":8,"avatarurl":"https://img.meituan.net/avatar/d4edccb850d8e766d40e26d5f15880c850470.jpg","nickName":"豆丁乖","score":3.5,"userId":262360895,"vipInfo":"","id":128545341,"content":"东方快车谋杀案，在一整部电影里，演员可以说是最亮眼的地方了。由于剧情的原因，每个角色在故事推进的过程中，都表现出了不同类型不同程度的悲恸。但每个人的表现方式都是不同的，但又各自切合角色。电影里很多画面构图都很美，尤其是剧情发展到后来，波洛穿过纷飞的大雪走向长桌，桌边人姿态表情各异，仿佛是在致敬最后的晚餐。因为原作实在是太经典了，以至于大家都知道这个故事是怎么回事，依然要去电影院观看！","time":"2017-11-10 10:34"},{"nick":"_qqn6h1438424789","approve":214,"oppose":0,"reply":9,"avatarurl":"https://img.meituan.net/avatar/b096b1b63bed0851d8115a4048246aad10613.jpg","nickName":"_qqn6h1438424789","score":5,"userId":239988954,"vipInfo":"","id":128270116,"content":"给我阿婆疯狂打call啊 真的是非常好的故事 看完书浑身鸡皮疙瘩都起来了","time":"2017-11-05 21:31"},{"nick":"德基广场","approve":161,"oppose":0,"reply":2,"avatarurl":"https://img.meituan.net/avatar/a0b700a8dd3b5d39b6012e15c4a0dac333160.jpg","nickName":"德基广场","score":5,"userId":6262637,"vipInfo":"","id":128276481,"content":"书相当好看！老版的电影也不错，那个胖胖的波洛大叔！这个大叔还帅点，期待啦！","time":"2017-11-05 22:25"},{"nick":"些许暖意","approve":162,"oppose":0,"reply":28,"avatarurl":"https://img.meituan.net/avatar/be1dc5b3474095c85beb3310652169ed56355.jpg","nickName":"些许暖意","score":0,"userId":45559477,"vipInfo":"","id":128291557,"content":"阿加莎关于波罗的故事我最喜欢的还是《阳光下的罪恶》和《尼罗河上的惨案》，对《东方快车谋杀案》很无感，但是冲着阿婆，也得去捧个场，这是阿婆的作品第一次登上国内大银幕呢😄","time":"2017-11-06 08:20"},{"nick":"banbi白","approve":71,"oppose":0,"reply":45,"avatarurl":"https://img.meituan.net/avatar/968c4a275fedf5e23b94e8fceeda3e5d262013.jpg","nickName":"banbi白","score":4,"userId":104846575,"vipInfo":"","id":128538114,"content":"嗯\u2026看过了消失的客人后，觉得套路差不多！某些自以为看过几本书的人不要和我bb没完了！就这部电影来说我个人的确更喜欢消失的客人！那些看过原著的别来和我显摆个没玩！这部电影我也给了八分！","time":"2017-11-10 15:38"},{"nick":"dorashiwo","approve":47,"oppose":0,"reply":6,"avatarurl":"https://img.meituan.net/avatar/7395fc7832028e31d52326d257800b4a12750.jpg","nickName":"dorashiwo","score":3.5,"userId":92232998,"vipInfo":"","id":128539007,"content":"不爱这个结局啊。还是喜欢出人意料反转再反转的凶手，比较刺激。这个结果，不大喜欢。不过影片拍的不错，很多画面挺美的，而且一镜到底真的很6。","time":"2017-11-10 04:27"}]
             * total : 14001
             * hasNext : true
             */

            private int total;
            private boolean hasNext;
            private List<CmtsBean> cmts;
            private List<HcmtsBean> hcmts;

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public boolean isHasNext() {
                return hasNext;
            }

            public void setHasNext(boolean hasNext) {
                this.hasNext = hasNext;
            }

            public List<CmtsBean> getCmts() {
                return cmts;
            }

            public void setCmts(List<CmtsBean> cmts) {
                this.cmts = cmts;
            }

            public List<HcmtsBean> getHcmts() {
                return hcmts;
            }

            public void setHcmts(List<HcmtsBean> hcmts) {
                this.hcmts = hcmts;
            }

            public static class CmtsBean {
                /**
                 * nick : 付冬100
                 * approve : 0
                 * oppose : 0
                 * reply : 0
                 * avatarurl : https://img.meituan.net/avatar/4773a4ecf10a47e30f34b4950ca748b5106237.jpg
                 * nickName : 付冬100
                 * score : 5
                 * userId : 75258634
                 * vipInfo :
                 * id : 128838594
                 * content : 一车人谋杀一个人
                 * time : 2017-11-13 14:03
                 */

                private String nick;
                private int approve;
                private int oppose;
                private int reply;
                private String avatarurl;
                private String nickName;
                private double score;
                private int userId;
                private String vipInfo;
                private int id;
                private String content;
                private String time;

                public String getNick() {
                    return nick;
                }

                public void setNick(String nick) {
                    this.nick = nick;
                }

                public int getApprove() {
                    return approve;
                }

                public void setApprove(int approve) {
                    this.approve = approve;
                }

                public int getOppose() {
                    return oppose;
                }

                public void setOppose(int oppose) {
                    this.oppose = oppose;
                }

                public int getReply() {
                    return reply;
                }

                public void setReply(int reply) {
                    this.reply = reply;
                }

                public String getAvatarurl() {
                    return avatarurl;
                }

                public void setAvatarurl(String avatarurl) {
                    this.avatarurl = avatarurl;
                }

                public String getNickName() {
                    return nickName;
                }

                public void setNickName(String nickName) {
                    this.nickName = nickName;
                }

                public double getScore() {
                    return score;
                }

                public void setScore(double score) {
                    this.score = score;
                }

                public int getUserId() {
                    return userId;
                }

                public void setUserId(int userId) {
                    this.userId = userId;
                }

                public String getVipInfo() {
                    return vipInfo;
                }

                public void setVipInfo(String vipInfo) {
                    this.vipInfo = vipInfo;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getContent() {
                    return content;
                }

                public void setContent(String content) {
                    this.content = content;
                }

                public String getTime() {
                    return time;
                }

                public void setTime(String time) {
                    this.time = time;
                }
            }

            public static class HcmtsBean {
                /**
                 * nick : tTS530336450
                 * approve : 533
                 * oppose : 0
                 * reply : 28
                 * avatarurl :
                 * nickName : tTS530336450
                 * score : 5
                 * userId : 215895676
                 * vipInfo :
                 * id : 128581795
                 * content : 第一次写电影的影评，送给东方快车谋杀案。
                 作为一个没有看过原作的观众，这部电影完美向我展示了这么一个严谨而又超乎寻常的几乎完美谋杀案，如果没有那么一场雪崩（当然没有如果）给10分，并非口味挑剔的专业观众或者领钱水军，一部电影能给我带来欣喜，惊叹，感动，反思，那么它就值得我的分数
                 * time : 2017-11-10 18:12
                 */

                private String nick;
                private int approve;
                private int oppose;
                private int reply;
                private String avatarurl;
                private String nickName;
                private double score;
                private int userId;
                private String vipInfo;
                private int id;
                private String content;
                private String time;

                public String getNick() {
                    return nick;
                }

                public void setNick(String nick) {
                    this.nick = nick;
                }

                public int getApprove() {
                    return approve;
                }

                public void setApprove(int approve) {
                    this.approve = approve;
                }

                public int getOppose() {
                    return oppose;
                }

                public void setOppose(int oppose) {
                    this.oppose = oppose;
                }

                public int getReply() {
                    return reply;
                }

                public void setReply(int reply) {
                    this.reply = reply;
                }

                public String getAvatarurl() {
                    return avatarurl;
                }

                public void setAvatarurl(String avatarurl) {
                    this.avatarurl = avatarurl;
                }

                public String getNickName() {
                    return nickName;
                }

                public void setNickName(String nickName) {
                    this.nickName = nickName;
                }

                public double getScore() {
                    return score;
                }

                public void setScore(double score) {
                    this.score = score;
                }

                public int getUserId() {
                    return userId;
                }

                public void setUserId(int userId) {
                    this.userId = userId;
                }

                public String getVipInfo() {
                    return vipInfo;
                }

                public void setVipInfo(String vipInfo) {
                    this.vipInfo = vipInfo;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getContent() {
                    return content;
                }

                public void setContent(String content) {
                    this.content = content;
                }

                public String getTime() {
                    return time;
                }

                public void setTime(String time) {
                    this.time = time;
                }
            }
        }
    }
}
