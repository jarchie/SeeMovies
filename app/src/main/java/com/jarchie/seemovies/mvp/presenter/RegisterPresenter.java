package com.jarchie.seemovies.mvp.presenter;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.widget.EditText;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.mvp.contact.RegisterContact;
import com.jarchie.seemovies.utils.TipUtils;
import com.jarchie.seemovies.helper.UserDBHelper;
import com.seemovies.sdk.mvp.base.BaseView;
import com.seemovies.sdk.mvp.impl.BasePresenterImpl;

/**
 * Created by Jarchie on 2017\11\17.
 * 描述：注册业务处理类
 */

@SuppressWarnings("unchecked")
public class RegisterPresenter extends BasePresenterImpl implements RegisterContact.presenter {
    public RegisterPresenter(BaseView view) {
        super(view);
    }

    @Override
    public void setHint(Context context, TextInputLayout userNameLayout, TextInputLayout passWordLayout) {
        userNameLayout.setHint(context.getResources().getString(R.string.user_name));
        passWordLayout.setHint(context.getResources().getString(R.string.user_password));
    }

    @Override
    public void register(Activity context, EditText editUserName, EditText editPwd) {
        UserDBHelper.insertUser(null, editUserName.getText().toString(), editPwd.getText().toString());
        if (UserDBHelper.queryBean(editUserName.getText().toString()) != null) {
            TipUtils.showToast(context, "注册成功");
            context.finish();
        } else {
            TipUtils.showToast(context, "注册失败");
        }
    }

}
