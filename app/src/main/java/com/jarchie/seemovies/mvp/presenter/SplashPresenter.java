package com.jarchie.seemovies.mvp.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.TextView;

import com.jarchie.seemovies.activity.MainActivity;
import com.jarchie.seemovies.mvp.contact.SplashContact;
import com.jarchie.seemovies.utils.Constant;
import com.seemovies.sdk.mvp.base.BaseView;
import com.seemovies.sdk.mvp.impl.BasePresenterImpl;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

/**
 * Created by Jarchie on 2017\11\21.
 * 描述：欢迎页面的业务处理类
 */

@SuppressWarnings("unchecked")
public class SplashPresenter extends BasePresenterImpl implements SplashContact.presenter {
    private int maxTime = 5;

    public SplashPresenter(BaseView view) {
        super(view);
    }

    @Override
    public void skipMainPage(final Context context, final TextView skipTv) {
        skipTv.setText(maxTime + " s");
        Observable.timer(Constant.SKIP_DELAY, TimeUnit.SECONDS)
                .repeat(Constant.SKIP_REPEAT)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(@NonNull Long aLong) throws Exception {
                        maxTime--;
                        skipTv.setText(maxTime + " s");
                        if (maxTime == 0) {
                            context.startActivity(new Intent(context, MainActivity.class));
                            ((Activity) context).finish();
                        }
                    }
                });
    }

    @Override
    public void clickSkipMainPage(Context context, TextView skipTv) {
        maxTime = 0;
        skipTv.setText(maxTime + " s");
        context.startActivity(new Intent(context, MainActivity.class));
        ((Activity) context).finish();
    }

}
