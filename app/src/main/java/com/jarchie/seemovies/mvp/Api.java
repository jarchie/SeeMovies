package com.jarchie.seemovies.mvp;

import com.seemovies.sdk.retrofit.BaseApiImpl;

/**
 * Created by Jarchie on 2017\11\7.
 * 描述：基础网络访问
 */

@SuppressWarnings("WeakerAccess")
public class Api extends BaseApiImpl {
    private static Api api = new Api(RetrofitService.BASE_URL);

    public Api(String baseUrl) {
        super(baseUrl);
    }

    public static RetrofitService getInstance() {
        return api.getRetrofit().create(RetrofitService.class);
    }

}
