package com.jarchie.seemovies.mvp.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jarchie.seemovies.activity.AboutActivity;
import com.jarchie.seemovies.activity.LoginActivity;
import com.jarchie.seemovies.activity.MyBlogActivity;
import com.jarchie.seemovies.activity.MyGiteeActivity;
import com.jarchie.seemovies.activity.MyGithubActivity;
import com.jarchie.seemovies.mvp.contact.MineContact;
import com.jarchie.seemovies.utils.Constant;
import com.jarchie.seemovies.utils.SharePreUtils;
import com.jarchie.seemovies.views.CustomDialog;
import com.seemovies.sdk.glide.GlideImageView;
import com.seemovies.sdk.mvp.base.BaseView;
import com.seemovies.sdk.mvp.impl.BasePresenterImpl;

import java.io.File;

/**
 * Created by Jarchie on 2017\11\17.
 * 描述：我的模块的业务处理类
 */

@SuppressWarnings("unchecked")
public class MinePresenter extends BasePresenterImpl implements MineContact.presenter {

    public MinePresenter(BaseView view) {
        super(view);
    }

    @Override
    public void initLoginState(Context context, TextView tvLogin, Button loginOutBtn) {
        if (SharePreUtils.getString(context, Constant.USERNAME, "").length() == 0) {
            SharePreUtils.putBoolean(context, Constant.LOGINED, false);
            tvLogin.setText("立即登录");
            loginOutBtn.setVisibility(View.GONE);
        } else {
            tvLogin.setText(SharePreUtils.getString(context, Constant.USERNAME, ""));
            loginOutBtn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void refreshLoginState(boolean loginState, Context context, TextView tvLogin, Button loginOutBtn) {
        if (loginState) {
            loginOutBtn.setVisibility(View.VISIBLE);
        } else {
            tvLogin.setText("立即登录");
            loginOutBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public void loginOut(Context context, TextView tvLogin, Button loginOutBtn) {
        SharePreUtils.deleShare(context, Constant.USERNAME);
        SharePreUtils.deleShare(context, Constant.PASSWORD);
        SharePreUtils.putBoolean(context, Constant.LOGINED, false);
        tvLogin.setText("立即登录");
        loginOutBtn.setVisibility(View.GONE);
        context.startActivity(new Intent(context, LoginActivity.class));
    }

    @Override
    public void intentLogin(Context context, String tag, boolean loginState, CustomDialog mCustomDialog) {
        if (!loginState) {
            context.startActivity(new Intent(context, LoginActivity.class));
        } else {
            switch (tag) {
                case Constant.INTENT_BLOG:
                    context.startActivity(new Intent(context, MyBlogActivity.class));
                    break;
                case Constant.INTENT_GITHUB:
                    context.startActivity(new Intent(context, MyGithubActivity.class));
                    break;
                case Constant.INTENT_GITEE:
                    context.startActivity(new Intent(context, MyGiteeActivity.class));
                    break;
                case Constant.INTENT_ABOUT:
                    context.startActivity(new Intent(context, AboutActivity.class));
                    break;
                case Constant.INTENT_AVATAR:
                    mCustomDialog.show();
                    break;
            }
        }
    }

    @Override
    public void openCamera(Activity context, CustomDialog mCustomDialog) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //判断内存卡是否可用,可用就进行存储
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(new File(Environment.getExternalStorageDirectory(), Constant.PHOTO_FILE_NAME)));
        context.startActivityForResult(intent, Constant.CAMERA_REQUEST_CODE);
        mCustomDialog.dismiss();
    }

    @Override
    public void openAlbum(Activity context, CustomDialog mCustomDialog) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        context.startActivityForResult(intent, Constant.ALBUM_REQUEST_CODE);
        mCustomDialog.dismiss();
    }

    @Override
    public void startPhotoZoom(Activity context, Uri uri) { //相机裁剪的数据
        if (uri == null) {
            return;
        }
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        //设置裁剪
        intent.putExtra("crop", true);
        //设置裁剪宽高比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        //设置裁剪图片的质量
        intent.putExtra("outputX", 300);
        intent.putExtra("outputY", 300);
        //发送数据
        intent.putExtra("return-data", true);
        context.startActivityForResult(intent, Constant.RESULT_REQUEST_CODE);
    }

    @Override
    public void setCircleImageView(GlideImageView imageView, Intent data) {
        Bundle bundle = data.getExtras();
        if (bundle != null) {
            Bitmap bitmap = bundle.getParcelable("data");
            imageView.setImageBitmap(bitmap);
        }
    }

}
