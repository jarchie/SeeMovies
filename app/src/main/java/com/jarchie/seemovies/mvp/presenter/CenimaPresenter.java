package com.jarchie.seemovies.mvp.presenter;

import com.jarchie.seemovies.mvp.Api;
import com.jarchie.seemovies.mvp.contact.CenimaContact;
import com.jarchie.seemovies.mvp.model.CenimaBean;
import com.seemovies.sdk.mvp.impl.BasePresenterImpl;
import com.seemovies.sdk.retrofit.ExceptionHelper;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Jarchie on 2017\11\20.
 * 描述：影院模块的处理类
 */

public class CenimaPresenter extends BasePresenterImpl<CenimaContact.view>
        implements CenimaContact.presenter {

    public CenimaPresenter(CenimaContact.view view) {
        super(view);
    }

    @Override
    public void getData() {
        Api.getInstance().cinemaList()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        addDisposable(disposable);
                        view.showLoading();
                    }
                })
                .map(new Function<CenimaBean, List<CenimaBean.DataBean.上城区Bean>>() {
                    @Override
                    public List<CenimaBean.DataBean.上城区Bean> apply(@NonNull CenimaBean cenimaBean) throws Exception {
                        return cenimaBean.getData().get上城区();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<CenimaBean.DataBean.上城区Bean>>() {
                    @Override
                    public void accept(@NonNull List<CenimaBean.DataBean.上城区Bean> list) throws Exception {
                        view.dismissLoading();
                        view.setData(list);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        view.dismissLoading();
                        ExceptionHelper.handleException(throwable);
                    }
                });
    }

}
