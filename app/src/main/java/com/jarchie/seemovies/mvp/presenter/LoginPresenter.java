package com.jarchie.seemovies.mvp.presenter;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.widget.CheckBox;
import android.widget.EditText;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.bean.RegisterEvent;
import com.jarchie.seemovies.bean.UserBean;
import com.jarchie.seemovies.mvp.contact.LoginContact;
import com.jarchie.seemovies.utils.Constant;
import com.jarchie.seemovies.utils.SharePreUtils;
import com.jarchie.seemovies.utils.TipUtils;
import com.jarchie.seemovies.helper.UserDBHelper;
import com.seemovies.sdk.mvp.base.BaseView;
import com.seemovies.sdk.mvp.impl.BasePresenterImpl;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Jarchie on 2017\11\17.
 * 描述：登录业务的处理类
 */

@SuppressWarnings("unchecked")
public class LoginPresenter extends BasePresenterImpl implements LoginContact.presenter {
    public LoginPresenter(BaseView view) {
        super(view);
    }

    @Override
    public void initHint(Context context, TextInputLayout userNameLayout, TextInputLayout passWordLayout) {
        userNameLayout.setHint(context.getResources().getString(R.string.user_name));
        passWordLayout.setHint(context.getResources().getString(R.string.user_password));
    }

    @Override
    public void initSaveState(Context context, CheckBox mCheckBox, EditText editUserName, EditText editPwd) {
        boolean isCheck = SharePreUtils.getBoolean(context, Constant.IS_SAVE, false);
        mCheckBox.setChecked(isCheck);
        if (isCheck) { //设置用户名和密码
            editUserName.setText(SharePreUtils.getString(context, Constant.USERNAME, ""));
            editPwd.setText(SharePreUtils.getString(context, Constant.PASSWORD, ""));
        }
    }

    @Override
    public void login(Activity context,CheckBox mCheckBox, EditText editUserName, EditText editPwd) {
        if (editUserName.getText().length() == 0 || editPwd.getText().length() == 0) {
            TipUtils.showToast(context, "请填写完整用户名或密码");
        } else {
            for (UserBean list : UserDBHelper.queryAllData()) {
                if (editUserName.getText().toString().equals(list.getUserName()) &&
                        editPwd.getText().toString().equals(list.getPassWord())) {
                    SharePreUtils.putBoolean(context, Constant.LOGINED, true);
                    /**保存密码****/
                    SharePreUtils.putBoolean(context, Constant.IS_SAVE, mCheckBox.isChecked());
                    //记住用户名和密码
                    if (mCheckBox.isChecked()) {
                        SharePreUtils.putString(context, Constant.USERNAME, editUserName.getText().toString());
                        SharePreUtils.putString(context, Constant.PASSWORD, editPwd.getText().toString());
                    } else {
                        SharePreUtils.deleShare(context, Constant.USERNAME);
                        SharePreUtils.deleShare(context, Constant.PASSWORD);
                    }
                    //利用EventBus发送事件
                    EventBus.getDefault().post(new RegisterEvent(editUserName.getText().toString()));
                    TipUtils.showToast(context, "登录成功");
                    context.finish();
                    break;
                } else {
                    TipUtils.showToast(context, "登录失败");
                }
            }
        }
    }

}
