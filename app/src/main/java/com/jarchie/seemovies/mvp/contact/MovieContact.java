package com.jarchie.seemovies.mvp.contact;

import com.jarchie.seemovies.mvp.model.MovieBean;
import com.seemovies.sdk.mvp.base.BasePresenter;
import com.seemovies.sdk.mvp.base.BaseView;

import java.util.List;

/**
 * Created by Jarchie on 2017\11\7.
 * 描述：首页列表数据契约类
 */

public interface MovieContact {

    interface view extends BaseView {
        /**
         * 设置数据
         *
         * @param indexList
         */
        void setData(List<MovieBean.DataBean.MoviesBean> indexList);
    }

    interface presenter extends BasePresenter {
        //获取数据
        void getData(int pageNum,int pageSize);
    }

}
