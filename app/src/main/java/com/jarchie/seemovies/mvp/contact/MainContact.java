package com.jarchie.seemovies.mvp.contact;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.jarchie.seemovies.activity.MainActivity;
import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationView;
import com.seemovies.sdk.mvp.base.BasePresenter;

/**
 * Created by Jarchie on 2017\11\24.
 * 描述：主页的契约类
 */

public interface MainContact {
    interface presenter extends BasePresenter {
        //初始化底部View
        void initBottomNavigationView(Context context,BottomNavigationView mBottomNavigationView);

        //处理Fragment的选择逻辑
        void setSelect(MainActivity context, int pos);

        //处理双击退出App
        void exitApp(MainActivity context);
    }
}
