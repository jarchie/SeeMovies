package com.jarchie.seemovies.mvp;

import com.jarchie.seemovies.mvp.model.CenimaBean;
import com.jarchie.seemovies.mvp.model.MovieBean;
import com.jarchie.seemovies.mvp.model.MovieDetailBean;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Jarchie on 2017\11\7.
 * 描述：接口URL类
 */

@SuppressWarnings("WeakerAccess")
public interface RetrofitService {
    //基础域名
    String BASE_URL = "http://m.maoyan.com/";

    //首页列表数据
    @GET("movie/list.json?type=hot")
    Observable<MovieBean> indexList(@Query("offset") int pageNum, @Query("limit") int pageSize);

    //详情页数据
    @GET("movie/{id}.json")
    Observable<MovieDetailBean> movieDetailList(@Path("id") int id);

    //影院（根据ip段查出本地影院）
    @GET("cinemas.json")
    Observable<CenimaBean> cinemaList();

}
