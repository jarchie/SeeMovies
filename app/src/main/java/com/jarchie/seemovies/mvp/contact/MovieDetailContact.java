package com.jarchie.seemovies.mvp.contact;

import com.jarchie.seemovies.mvp.model.MovieDetailBean;
import com.seemovies.sdk.mvp.base.BasePresenter;
import com.seemovies.sdk.mvp.base.BaseView;

import java.util.List;

/**
 * Created by Jarchie on 2017\11\9.
 * 描述：电影详情页的契约类
 */

public interface MovieDetailContact {

    interface view extends BaseView {
        /**
         * 设置数据
         *
         * @param movieDetailList
         */
        void setData(MovieDetailBean.DataBean movieDetailList);
    }

    interface presenter extends BasePresenter {
        //获取数据
        void getData(int id);
    }

}
