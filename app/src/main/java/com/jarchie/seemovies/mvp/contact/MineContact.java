package com.jarchie.seemovies.mvp.contact;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Button;
import android.widget.TextView;

import com.jarchie.seemovies.views.CustomDialog;
import com.seemovies.sdk.glide.GlideImageView;
import com.seemovies.sdk.mvp.base.BasePresenter;

/**
 * Created by Jarchie on 2017\11\17.
 * 描述：我的模块的契约类
 */

public interface MineContact {
    interface presenter extends BasePresenter {
        //初始化登录
        void initLoginState(Context context, TextView tvLogin, Button loginOutBtn);

        //刷新登录状态，更新UI
        void refreshLoginState(boolean loginState, Context context, TextView tvLogin, Button loginOutBtn);

        //退出逻辑
        void loginOut(Context context, TextView tvLogin, Button loginOutBtn);

        //登录逻辑
        void intentLogin(Context context, String tag, boolean loginState, CustomDialog mCustomDialog);

        //打开相机
        void openCamera(Activity context, CustomDialog mCustomDialog);

        //打开相册
        void openAlbum(Activity context, CustomDialog mCustomDialog);

        //裁剪图片的方法
        void startPhotoZoom(Activity context, Uri uri);

        //设置裁剪后的图片到ImageView上面
        void setCircleImageView(GlideImageView imageView, Intent data);
    }
}
