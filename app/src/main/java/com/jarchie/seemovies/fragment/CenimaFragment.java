package com.jarchie.seemovies.fragment;

import android.app.ProgressDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.adapter.CenimaListAdapter;
import com.jarchie.seemovies.mvp.contact.CenimaContact;
import com.jarchie.seemovies.mvp.model.CenimaBean;
import com.jarchie.seemovies.mvp.presenter.CenimaPresenter;
import com.seemovies.sdk.mvp.base.BaseFragment;

import java.util.List;

/**
 * Created by Jarchie on 2017\11\9.
 * 描述：影院模块的Fragment
 */

@SuppressWarnings("deprecation")
public class CenimaFragment extends BaseFragment<CenimaContact.presenter> implements CenimaContact.view {
    private RecyclerView mRecyclerView;
    private ProgressDialog mProgressDialog;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_cenima;
    }

    @Override
    protected void initView(View rootView) {
        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setMessage("加载中。。。");
        mRecyclerView = obtainView(R.id.cenima_recycler);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setHasFixedSize(true);
    }

    @Override
    protected void setListener() {

    }

    @Override
    protected void initData() {
        presenter.getData();
    }

    @Override
    public CenimaContact.presenter initPresenter() {
        return new CenimaPresenter(this);
    }

    @Override
    public void setData(List<CenimaBean.DataBean.上城区Bean> list) {
        mRecyclerView.setAdapter(new CenimaListAdapter(getContext(), list));
    }

    @Override
    public void showLoading() {
        super.showLoading();
        mProgressDialog.show();
    }

    @Override
    public void dismissLoading() {
        super.dismissLoading();
        mProgressDialog.dismiss();
    }

}
