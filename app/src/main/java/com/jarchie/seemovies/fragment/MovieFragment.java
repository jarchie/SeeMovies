package com.jarchie.seemovies.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.activity.MovieDetailActivity;
import com.jarchie.seemovies.adapter.IndexListAdapter;
import com.jarchie.seemovies.mvp.contact.MovieContact;
import com.jarchie.seemovies.mvp.model.MovieBean;
import com.jarchie.seemovies.mvp.presenter.MoviePresenter;
import com.jarchie.seemovies.utils.Constant;
import com.jarchie.seemovies.helper.RefreshInitView;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.seemovies.sdk.mvp.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jarchie on 2017\11\9.
 * 描述：创建电影模块的Fragment
 */

@SuppressWarnings("deprecation")
public class MovieFragment extends BaseFragment<MovieContact.presenter> implements MovieContact.view, OnRefreshListener, OnLoadmoreListener {
    private RefreshLayout mRefreshLayout;
    private RecyclerView mRecyclerView;
    private List<MovieBean.DataBean.MoviesBean> mList = new ArrayList<>(); //数据源
    private IndexListAdapter mAdapter;
    private int pageNum = 0;
    private ProgressDialog dialog;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_movie;
    }

    @Override
    protected void initView(View rootView) {
        dialog = new ProgressDialog(getContext());
        dialog.setMessage("加载中。。。");
        mRefreshLayout = obtainView(R.id.refreshLayout);
        mRecyclerView = obtainView(R.id.index_recycler);
        RefreshInitView.initView(mRefreshLayout);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setHasFixedSize(true);
    }

    @Override
    protected void setListener() {
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setOnLoadmoreListener(this);
    }

    @Override
    protected void initData() {
        mRefreshLayout.autoRefresh();
        RefreshInitView.initDataView(mRefreshLayout, getActivity());
        mAdapter = new IndexListAdapter(getContext(), mList);
        mRecyclerView.setAdapter(mAdapter);
        presenter.getData(pageNum, Constant.PAGE_SIZE);
        mAdapter.setOnItemClickListener(new IndexListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int postion, int id) {
                Intent intent = new Intent(getContext(), MovieDetailActivity.class);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });
    }

    @Override
    public MovieContact.presenter initPresenter() {
        return new MoviePresenter(this);
    }

    @Override
    public void setData(List<MovieBean.DataBean.MoviesBean> indexList) {
        mList.addAll(indexList);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        mRefreshLayout.finishLoadmore();
        presenter.getData(++pageNum, Constant.PAGE_SIZE);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        mRefreshLayout.finishRefresh();
        mList.clear();
        pageNum = 0;
        presenter.getData(pageNum, Constant.PAGE_SIZE);
    }

    @Override
    public void showLoading() {
        super.showLoading();
        dialog.show();
    }

    @Override
    public void dismissLoading() {
        super.dismissLoading();
        dialog.dismiss();
    }

}
