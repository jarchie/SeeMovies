package com.jarchie.seemovies.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.bean.RegisterEvent;
import com.jarchie.seemovies.mvp.contact.MineContact;
import com.jarchie.seemovies.mvp.presenter.MinePresenter;
import com.jarchie.seemovies.utils.Constant;
import com.jarchie.seemovies.utils.ImageUtils;
import com.jarchie.seemovies.utils.SharePreUtils;
import com.jarchie.seemovies.views.CustomDialog;
import com.seemovies.sdk.glide.GlideImageView;
import com.seemovies.sdk.mvp.base.BaseFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;

/**
 * Created by Jarchie on 2017\11\9.
 * 描述：我的模块的Fragment
 */

@SuppressWarnings({"unused", "ResultOfMethodCallIgnored"})
public class MineFragment extends BaseFragment<MineContact.presenter> implements View.OnClickListener {
    private RelativeLayout loginLayout, myblogLayout, mygithubLayout, mygiteeLayout, aboutLayout;
    private GlideImageView mImageView;
    private CustomDialog mCustomDialog;
    private Button cameraBtn, picBtn, cancelBtn, loginOutBtn;
    private TextView tvLogin;
    private boolean loginState;
    private File tempFile = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this); //注册成为订阅者
    }

    @Override
    public void onResume() { //刷新登录状态
        super.onResume();
        loginState = SharePreUtils.getBoolean(getContext(), Constant.LOGINED, false);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void initView(View rootView) {
        loginLayout = obtainView(R.id.layout_login);
        myblogLayout = obtainView(R.id.layout_my_blog);
        mygithubLayout = obtainView(R.id.layout_my_github);
        mygiteeLayout = obtainView(R.id.layout_my_gitee);
        aboutLayout = obtainView(R.id.layout_about);
        mImageView = obtainView(R.id.user_avatar);
        tvLogin = obtainView(R.id.tv_login);
        loginOutBtn = obtainView(R.id.btn_login_out);
        //获取图片
        ImageUtils.getImageFromShareUtil(getContext(),mImageView);
        //初始化Dialog
        mCustomDialog = new CustomDialog(getContext(), ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, R.layout.dialog_avatar, R.style.Theme_dialog, Gravity.BOTTOM);
        mCustomDialog.setCancelable(false);
        cameraBtn = mCustomDialog.findViewById(R.id.btn_camera);
        picBtn = mCustomDialog.findViewById(R.id.btn_picture);
        cancelBtn = mCustomDialog.findViewById(R.id.btn_cancel);
    }

    @Override
    protected void setListener() {
        loginLayout.setOnClickListener(this);
        myblogLayout.setOnClickListener(this);
        mygithubLayout.setOnClickListener(this);
        mygiteeLayout.setOnClickListener(this);
        aboutLayout.setOnClickListener(this);
        mImageView.setOnClickListener(this);
        cameraBtn.setOnClickListener(this);
        picBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        loginOutBtn.setOnClickListener(this);
    }

    @Override
    protected void initData() {
        presenter.initLoginState(getContext(), tvLogin, loginOutBtn);
    }

    @Override
    public MineContact.presenter initPresenter() {
        return new MinePresenter(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_login: //登录
                presenter.intentLogin(getContext(), "", loginState, mCustomDialog);
                break;
            case R.id.layout_my_blog: //博客
                presenter.intentLogin(getContext(), Constant.INTENT_BLOG, loginState, mCustomDialog);
                break;
            case R.id.layout_my_github: //Github
                presenter.intentLogin(getContext(), Constant.INTENT_GITHUB, loginState, mCustomDialog);
                break;
            case R.id.layout_my_gitee: //码云
                presenter.intentLogin(getContext(), Constant.INTENT_GITEE, loginState, mCustomDialog);
                break;
            case R.id.layout_about: //关于
                presenter.intentLogin(getContext(), Constant.INTENT_ABOUT, loginState, mCustomDialog);
                break;
            case R.id.user_avatar: //头像
                presenter.intentLogin(getContext(), Constant.INTENT_AVATAR, loginState, mCustomDialog);
                break;
            case R.id.btn_camera: //拍照
                presenter.openCamera(getActivity(), mCustomDialog);
                break;
            case R.id.btn_picture: //选择相册
                presenter.openAlbum(getActivity(), mCustomDialog);
                break;
            case R.id.btn_cancel: //取消
                mCustomDialog.dismiss();
                break;
            case R.id.btn_login_out: //退出登录
                presenter.loginOut(getContext(), tvLogin, loginOutBtn);
                break;
        }
    }

    //处理相机和相册返回的数据
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != Constant.RESULT_CANCELED) {
            switch (requestCode) {
                case Constant.CAMERA_REQUEST_CODE: //拍照
                    tempFile = new File(Environment.getExternalStorageDirectory(), Constant.PHOTO_FILE_NAME);
                    presenter.startPhotoZoom(getActivity(), Uri.fromFile(tempFile));
                    break;
                case Constant.ALBUM_REQUEST_CODE: //相册
                    if (data != null) {
                        presenter.startPhotoZoom(getActivity(), data.getData());
                    }
                    break;
                case Constant.RESULT_REQUEST_CODE: //结果
                    if (data != null) { //有可能点击取消，防止空指针导致的程序Crash
                        presenter.setCircleImageView(mImageView, data); //拿到图片设置
                        if (tempFile != null) { //设置完成图片之后，将之前的图片删除
                            tempFile.delete();
                        }
                    }
                    break;
            }
        }
    }

    //订阅方法，当接收到事件时会调用这个方法
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRegisterEvent(RegisterEvent event) {
        tvLogin.setText(event.getMessage());
        loginState = SharePreUtils.getBoolean(getContext(), Constant.LOGINED, false);
        presenter.refreshLoginState(loginState, getContext(), tvLogin, loginOutBtn);
    }

    @Override
    public void onDestroy() { //取消注册
        super.onDestroy();
        ImageUtils.putImageToShareUtil(getContext(),mImageView);
        EventBus.getDefault().unregister(this);
    }

}
