package com.jarchie.seemovies.fragment;

import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.helper.LoadingH5Page;
import com.jarchie.seemovies.utils.CommonUtils;
import com.jarchie.seemovies.utils.Constant;
import com.seemovies.sdk.mvp.base.BaseFragment;
import com.seemovies.sdk.mvp.base.BasePresenter;
import com.seemovies.sdk.utils.BackHandlerHelper;

/**
 * Created by Jarchie on 2017\11\21.
 * 描述：测试封装的Fragment的返回键处理WebView是否好用
 * 这里给大家加载的是鸿洋大神的网站玩安卓，推荐给大家
 * 地址：http://www.wanandroid.com/
 */

@SuppressWarnings("deprecation")
public class WanAndroidFragment extends BaseFragment implements View.OnClickListener {
    private Toolbar mToolBar;
    private ProgressBar mProgressbar;
    private WebView mWebview;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_wanandroid;
    }

    @Override
    protected void initView(View rootView) {
        mToolBar = (Toolbar) obtainView(R.id.toolbar);
        mProgressbar = (ProgressBar) obtainView(R.id.mProgressBar);
        mWebview = (WebView) obtainView(R.id.mWebView);
    }

    @Override
    protected void setListener() {
        mToolBar.setOnClickListener(this);
    }

    @Override
    protected void initData() {
        LoadingH5Page.handleWebView(mWebview, mProgressbar, Constant.WAN_ANDROID);
    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar: //自定义的返回按钮
                CommonUtils.backPreviousPage(getContext(), Constant.BACK_PREVIOUS_FRAGMENT, KeyEvent.KEYCODE_BACK, mWebview);
                break;
        }
    }

    //处理系统返回键
    @Override
    public boolean onBackPressed() {
        if (mWebview.canGoBack()) {
            mWebview.goBack();
            return true;
        } else {
            return BackHandlerHelper.handleBackPress(this);
        }
    }

}
