package com.jarchie.seemovies.utils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import java.lang.ref.WeakReference;

/**
 * Created by Jarchie on 2017\11\7.
 * 描述：获取屏幕宽高的帮助类
 */

@SuppressWarnings({"unused", "WeakerAccess"})
public class ScreenSizeUtils {
    private static ScreenSizeUtils mInstance = null;
    private int screenWidth, screenHeigth;

    public static ScreenSizeUtils getInstance(Context mContext) {
        if (mInstance == null) {
            synchronized (ScreenSizeUtils.class) {
                if (mInstance == null)
                    mInstance = new ScreenSizeUtils(mContext);
            }
        }
        return mInstance;
    }

    private ScreenSizeUtils(Context mContext) {
        WeakReference<Context> contextWeakReference = new WeakReference<>(mContext);
        Context context = contextWeakReference.get();
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(dm);
        screenWidth = dm.widthPixels;// 获取屏幕分辨率宽度
        screenHeigth = dm.heightPixels;
    }

    //获取屏幕宽度
    public int getScreenWidth() {
        return screenWidth;
    }

    //获取屏幕高度
    public int getScreenHeight() {
        return screenHeigth;
    }

}
