package com.jarchie.seemovies.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.TextView;

/**
 * Created by Jarchie on 2017\11\21.
 * 描述：通用工具方法
 */

public class CommonUtils {

    /**
     * 自定义方法处理WebView的返回键，每次返回上一级页面
     *
     * @param context
     * @param keyCode
     * @param webView
     */
    public static void backPreviousPage(Context context, int flag, int keyCode, WebView webView) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            switch (flag) {
                case Constant.BACK_PREVIOUS_ACTIVITY:
                    if (webView.canGoBack()) {
                        webView.goBack();//返回上一页面
                    } else {
                        ((Activity) context).finish();
                    }
                    break;
                case Constant.BACK_PREVIOUS_FRAGMENT:
                    if (webView.canGoBack()) {
                        webView.goBack();//返回上一页面
                    }
                    break;
            }
        }
    }

    /**
     * 设置字体
     *
     * @param mContext
     * @param textView
     */
    public static void setFont(Context mContext, TextView textView) {
        Typeface fontType = Typeface.createFromAsset(mContext.getAssets(), "fonts/FONT.TTF");
        textView.setTypeface(fontType);
    }

}
