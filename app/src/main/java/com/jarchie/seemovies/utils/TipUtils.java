package com.jarchie.seemovies.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Jarchie on 2017\11\9.
 * 描述：提示信息工具类
 */

public class TipUtils {
    private static Toast toast;

    public static void showToast(Context context, String content) {
        if (toast == null) {
            toast = Toast.makeText(context, content, Toast.LENGTH_SHORT);
        } else {
            toast.setText(content);
        }
        toast.show();
    }

}
