package com.jarchie.seemovies.utils;

/**
 * Created by Jarchie on 2017\11\8.
 * 描述：项目中使用的常量类
 */

@SuppressWarnings("WeakerAccess")
public class Constant {
    //每页的数据量
    public static final int PAGE_SIZE = 10;

    //Back键退出应用所需时间
    public static final int EXIT_TIME = 2000;

    //是否记住密码的选中状态
    public static final String IS_SAVE = "savepwd";

    //登录状态
    public static final String LOGINED = "islogin";

    //用户名key
    public static final String USERNAME = "username";

    //密码key
    public static final String PASSWORD = "password";

    //圆形头像key
    public static final String IMAGE_TITLE = "image_title";

    //我的页面判断跳转的五种情况:博客、GitHub、码云、关于、用户头像
    public static final String INTENT_BLOG = "blog";
    public static final String INTENT_GITHUB = "github";
    public static final String INTENT_GITEE = "gitee";
    public static final String INTENT_ABOUT = "about";
    public static final String INTENT_AVATAR = "avatar";

    //照片名称
    public static final String PHOTO_FILE_NAME = "fileImage.jpg";

    //请求码:拍照、相册、结果
    public static final int RESULT_CANCELED = 0;
    public static final int CAMERA_REQUEST_CODE = 1;
    public static final int ALBUM_REQUEST_CODE = 2;
    public static final int RESULT_REQUEST_CODE = 3;

    //Blog地址
    public static final String MYBLOG_URL = "http://blog.csdn.net/jarchie520";

    //GitHub地址
    public static final String MYGIHUB_URL = "https://github.com/JArchie";

    //Gitee地址
    public static final String MYGITEE_URL = "https://gitee.com/jarchie/projects?scope=personal";

    //玩安卓网站地址
    public static final String WAN_ANDROID = "http://www.wanandroid.com/";

    //处理Activity和Fragment中的WebView返回键
    public static final int BACK_PREVIOUS_ACTIVITY = 1;
    public static final int BACK_PREVIOUS_FRAGMENT = 2;

    //跳转延时的常量
    public static final int SKIP_DELAY = 1;

    //重复发射数据的常量
    public static final int SKIP_REPEAT = 5;

}
