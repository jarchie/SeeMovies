package com.jarchie.seemovies;

import android.app.Application;
import android.content.Context;

import com.jarchie.seemovies.gen.GreenDaoManager;

/**
 * Created by Jarchie on 2017\11\8.
 * 描述：项目全局初始化类
 */

@SuppressWarnings("unused")
public class BaseApplication extends Application {
    private static Context mContext;
    private static BaseApplication mInstance = null;

    public BaseApplication() {}

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        GreenDaoManager.getInstance();
    }

    //采用单例模式构建公共静态返回本类对象的方法
    public static BaseApplication getInstance() {
        if (mInstance == null) {
            synchronized (BaseApplication.class) {
                mInstance = new BaseApplication();
            }
        }
        return mInstance;
    }

    //获取全局的上下文对象
    public static Context getAppContext() {
        return mContext;
    }

}
