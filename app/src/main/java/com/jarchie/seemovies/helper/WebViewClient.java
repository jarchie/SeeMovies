package com.jarchie.seemovies.helper;

import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

/**
 * Created by Jarchie on 2017\11\20.
 * 描述：用于WebView页面中，自定义的WebViewClient
 */

@SuppressWarnings("WeakerAccess")
public class WebViewClient extends WebChromeClient {
    private ProgressBar mProgressBar;

    public WebViewClient(ProgressBar progressBar) {
        this.mProgressBar = progressBar;
    }

    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        super.onProgressChanged(view, newProgress);
        //处理进度,加载完成时隐藏进度条
        if (newProgress == 100) {
            mProgressBar.setVisibility(View.GONE);
        }
    }

}
