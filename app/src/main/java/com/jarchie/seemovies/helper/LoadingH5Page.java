package com.jarchie.seemovies.helper;

import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

/**
 * Created by Jarchie on 2017\11\20.
 * 描述：加载处理H5页面
 */

@SuppressWarnings("deprecation")
public class LoadingH5Page {

    /**
     * 公共的加载WebView的方法，这里统一处理，避免代码冗余
     *
     * @param mWebview
     * @param mProgressbar
     * @param url
     */
    public static void handleWebView(WebView mWebview, ProgressBar mProgressbar, final String url) {
        //加载网页的处理逻辑
        mWebview.getSettings().setJavaScriptEnabled(true);
        //支持缩放
        mWebview.getSettings().setSupportZoom(true);
        mWebview.getSettings().setBuiltInZoomControls(true);
        //设置自适应任意大小的网页
        mWebview.getSettings().setUseWideViewPort(true);
        mWebview.getSettings().setLoadWithOverviewMode(true);
        mWebview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        //接口回调
        mWebview.setWebChromeClient(new WebViewClient(mProgressbar));
        //加载网页
        mWebview.loadUrl(url);
        //本地显示
        mWebview.setWebViewClient(new android.webkit.WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(url);
                //接收这个事件
                return true;
            }
        });
    }

}
