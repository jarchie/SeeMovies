package com.jarchie.seemovies.bean;

/**
 * Created by Jarchie on 2017\11\16.
 * 描述：创建事件实体类
 */

public class RegisterEvent {
    private String message;
    private String imgUrl;

    public RegisterEvent() {}

    public RegisterEvent(String message) {
        this.message = message;
    }

    public RegisterEvent(String message, String imgUrl) {
        this.message = message;
        this.imgUrl = imgUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
