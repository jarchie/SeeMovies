package com.jarchie.seemovies.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.mvp.model.CenimaBean;

import java.util.List;

/**
 * Created by Jarchie on 2017\11\20.
 * 描述：影院列表适配器
 */

public class CenimaListAdapter extends RecyclerView.Adapter<CenimaListAdapter.ViewHolder> {
    private Context mContext;
    private List<CenimaBean.DataBean.上城区Bean> mList;

    public CenimaListAdapter(Context context,List<CenimaBean.DataBean.上城区Bean> list){
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_cenima_list_adapter,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.name.setText(mList.get(position).getNm());
        holder.address.setText(mList.get(position).getAddr());
        holder.area.setText(mList.get(position).getArea());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @SuppressWarnings("WeakerAccess")
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,address,area;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_cenima_name);
            address = itemView.findViewById(R.id.item_cenima_address);
            area = itemView.findViewById(R.id.item_cenima_area);
        }
    }

}
