package com.jarchie.seemovies.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.mvp.model.MovieDetailBean;
import com.seemovies.sdk.glide.GlideImageView;

import java.util.List;

/**
 * Created by Jarchie on 2017\11\14.
 * 描述：首页列表数据适配器
 */

public class IndexCommentAdapter extends RecyclerView.Adapter<IndexCommentAdapter.ViewHolder> {
    private List<MovieDetailBean.DataBean.CommentResponseModelBean.HcmtsBean> mList;
    private Context mContext;

    public IndexCommentAdapter(Context context, List<MovieDetailBean.DataBean.CommentResponseModelBean.HcmtsBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_comment_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.name.setText(mList.get(position).getNickName());
        holder.time.setText(mList.get(position).getTime());
        holder.content.setText(mList.get(position).getContent());
        holder.avatar.loadImage(mList.get(position).getAvatarurl(), R.color.placeholder_color);
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, time, content;
        GlideImageView avatar;

        ViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.comment_image);
            name = itemView.findViewById(R.id.comment_name);
            time = itemView.findViewById(R.id.comment_time);
            content = itemView.findViewById(R.id.comment_content);
        }
    }

}
