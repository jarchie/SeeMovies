package com.jarchie.seemovies.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jarchie.seemovies.R;
import com.jarchie.seemovies.mvp.model.MovieBean;
import com.seemovies.sdk.glide.GlideImageView;

import java.util.List;

/**
 * Created by Jarchie on 2017\11\7.
 * 描述：首页列表数据适配器
 */

public class IndexListAdapter extends RecyclerView.Adapter<IndexListAdapter.ViewHolder> {
    private List<MovieBean.DataBean.MoviesBean> mList;
    private Context mContext;
    //事件回调的监听
    private IndexListAdapter.OnItemClickListener onItemClickListener;

    public IndexListAdapter(Context context, List<MovieBean.DataBean.MoviesBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    //设置回调监听
    public void setOnItemClickListener(IndexListAdapter.OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_index_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mShowInfo.setText(mList.get(position).getShowInfo());
        holder.mName.setText(mList.get(position).getNm());
        holder.mCat.setText(mList.get(position).getCat());
        holder.imageView.loadImage(mList.get(position).getImg(), R.color.placeholder_color);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    int pos = holder.getLayoutPosition();
                    int id = mList.get(pos).getId();
                    onItemClickListener.onItemClick(holder.itemView, pos,id);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mShowInfo, mName, mCat;
        GlideImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.index_image);
            mShowInfo = itemView.findViewById(R.id.item_index_showinfo);
            mName = itemView.findViewById(R.id.item_index_name);
            mCat = itemView.findViewById(R.id.item_index_cat);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int postion,int id);
    }

}
